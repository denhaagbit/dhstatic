/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                               */
/*                                                                          */
/* Filename:                $File: dh-scripts.js $               */
/* Last changed by:         $Author: bslokxr $                              */
/* Last modified:           $Date: 2011-12-14 09:02:47 +0100 (wo, 14 dec 2011) $                                          */
/* Revision number:         $Rev: 31 $                                      */
/*                                                                          */
/****************************************************************************/

/*
 *  Tracking google analytics 
 */
(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    //ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.denhaag.nl/static/denhaagpresentation/js/ga.js';
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://') + 'localhost.denhaag.dev/static/denhaagpresentation/js/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

$(document).ready(function() {
    // downloads
    $("a[href*='/file?']").attr("onClick","_gaq.push(['_trackPageview', this.href]);");
    // external links
    $("a[href*='http']").not("a[href*='www.denhaag.nl']").not("a[href*='redactie.denhaag.nl']")
        .each(function(i,link)
        { 
            var link = $(this);
            if (this.onclick == undefined || this.onclick == null) {
                link.attr("onClick","_gaq.push(['_trackPageview', this.href]);");
            } else {
                link.bind('click',function() {
                    _gaq.push(['_trackPageview', this.href]);
                });
            }
        });
});

/*
 * New unobtrusive application function library based on plugin structures
 */
var denhaag = {};

(function($) {
    
    denhaag.plugins = 
    {
        init : function ()
        {
            // Helper functions and classes
            if (document.getElementsByTagName)
            {
                document.getElementsByTagName('html')[0].className = "js-on";
            };
            // Bind dialog
            if($('.dh-bh-dialog').length)
            {
                denhaag.plugins.dialog.init();
            };
        }
    };
    
    /*
     * Den Haag: create rich dialog function
     */
    denhaag.plugins.dialog =
    {
        init : function ()
        {
            $('a.dh-bh-dialog').live('click', function () {
                if ($(this).attr('href').length) {                    
                    denhaag.plugins.dialog.write(this);
                    return false;
                }
            });
            
        },
        write : function (obj)
        {
            $($(obj).attr('href')).dialog({
                autoOpen: false,
                width: 768,
                height: 500,
                closeText: 'Sluit',
                draggable: false,
                resizable: false,
                modal: true,
                open: function () {
                    $(".ui-widget-overlay").click(function()
                    {
                        $($(obj).attr('href')).dialog('close');
                    });
                }
            });
            
            $($(obj).attr('href')).dialog('open');
        }
    };
    
    // Initialize denhaag plugins
    denhaag.plugins.init();
    
})(jQuery);


/*
 *  jq.extend.js
 */
if(jQuery) (function($){
    $.event.special.imgload={
        add:function(hollaback){
            if(this.nodeType===1&&this.tagName.toLowerCase()==='img'&&this.src!==''){
                if(this.complete||this.readyState===4){
                    hollaback.handler.apply(this);
                }else if(this.readyState==='uninitialized'&&this.src.indexOf('data:')===0){
                    $(this).trigger('error');
                }else{
                    $(this).bind('load', hollaback.handler);
                }
            }
        }
    };
    $.fn.carouselFotoDh = function(params){
        var conf = $.extend({
            clipBlock:'.clipCarousel',
            moveBlock:'ul',
            itemBlock:'li',
            prevPage:'itemArrowLeft',
            nextPage:'itemArrowRight',
            viewItem:'unitSingle',
            currentBlock:'unitCurrent',
            playBlock:'unitSlide',
            playDuration:6000,
            timeAnimate:300,
            nWidth:707,
            nHeight:472,
            labelPlay:'',
            labelStop:'',
            labelPrev:'',
            labelNext:'',
            labelFotoImg:'',
            labelFromImg:''
        }, params);
        return this.each(function(){
            var c=conf, o=$(this), f=this, t1=null, view=$('<div />').addClass(c.viewItem), fake=$('<div />').addClass('unitFake'), prev=$('<a />').addClass(c.prevPage).attr('title', c.labelPrev).html(c.labelPrev),
                next=$('<a />').addClass(c.nextPage).attr('title', c.labelNext).html(c.labelNext), currentPosition, currentItem=-1, preload=[], cur=$('<div />').addClass(c.currentBlock), slide=$('<a href="#" title="'+c.labelPlay+'">'+c.labelPlay+'</a>').addClass(c.playBlock), slideInfo=false, fakeList=$('<div />');
            $.extend(f,{
                getClip:function(){return o.find(c.clipBlock)},
                getMoveBlock:function(){return f.getClip().find(c.moveBlock)},
                getLastItem:function(){
                    var p=f.getAllItem().eq(f.getSizeItem()-1);
                    return f.getClip().outerWidth(false)-(p.outerWidth(false)+p.position().left)},
                getCenterItem:function(itemIn){
                    var p=f.getAllItem().eq(itemIn);
                    return p.outerWidth(false)/2+p.position().left;
                },
                getSizeItem:function(){return f.getAllItem().size()},
                getAllItem:function(){return f.getMoveBlock().children(c.itemBlock)},
                getFocus:function(i){return fakeList.children('a').eq(i).focus()},
                pushAll:function(){
                    f.getAllItem().each(function(i){
                        var desc=$('<div />').addClass('unitDesc');
                        preload.push(desc.append($(this).find('.unitText')));
                    });
                },
                moveItem:function(itemIn){
                    currentPosition=-f.getCenterItem(itemIn)+f.getClip().outerWidth(false)/2;
                    prev.removeClass('disableArrow'); next.removeClass('disableArrow');
                    if(currentPosition<f.getLastItem()){currentPosition=f.getLastItem(); next.addClass('disableArrow')}
                    if(currentPosition>0){currentPosition=0; prev.addClass('disableArrow')}
                    if(currentItem!=itemIn){
                        f.getAllItem().removeClass('itemActive');
                        currentItem=itemIn;
                        f.getMoveBlock().animate({left:currentPosition});
                        f.loadingItem(itemIn);
                        f.viewCurrent(currentItem);
                    }
                    f.getAllItem().eq(currentItem).addClass('itemActive');
                },
                resizeImg:function(imgSub, needHeight, needWidth){
                    var aspectRatio=needWidth/needHeight, thisWidth=imgSub.width(), thisHeight=imgSub.height(), imgAspectRatio=thisWidth/thisHeight, topSize=0, okHeight=needHeight;
                        if(imgAspectRatio>aspectRatio){needHeight=Math.floor(thisHeight/thisWidth*needWidth);
                        }else{needWidth=Math.floor(thisWidth/thisHeight*needHeight)}
                        if(needHeight<okHeight){topSize=(okHeight-needHeight)/2}
                        $(imgSub).css({height:needHeight, width:needWidth, marginTop:topSize});
                },
                loadingItem:function(itemLink){
                    fake.removeClass('hideElement').animate({opacity:1}, c.timeAnimate, function(){
                        $('<img />').attr('src', f.getAllItem().eq(itemLink).find('a').attr('href')).bind('imgload', function(e){
                                view.empty().append(preload[itemLink]);
                                f.resizeImg($(this).appendTo(view), c.nHeight-preload[itemLink].height(), c.nWidth);
                                $(this).appendTo(view);
                                fake.animate({opacity:0}, c.timeAnimate, function(){fake.addClass('hideElement')});
                            });
                    });
                },
                movePage:function(toSide){
                    if(toSide=='minus'){
                        currentPosition+=f.getClip().outerWidth(false);
                        if(currentPosition>0){currentPosition=0; prev.addClass('disableArrow')}
                    }else if(toSide=='plus'){
                        currentPosition-=f.getClip().outerWidth(false);
                        if(currentPosition<f.getLastItem()){currentPosition=f.getLastItem(); next.addClass('disableArrow')}
                    }
                    f.getMoveBlock().animate({left:currentPosition});
                },
                slideShow:function(){
                    if(slideInfo==false){
                        slide.addClass('startSlide').html(c.labelStop).attr('title', c.labelStop);
                        slideInfo=true; f.autoRestart();
                        f.getClip().bind('mouseenter', function(){f.autoPause()}).bind('mouseleave',function(){f.autoRestart()});
                        view.bind('mouseenter', function(){f.autoPause()}).bind('mouseleave',function(){f.autoRestart()});
                    }else{
                        slide.removeClass('startSlide').html(c.labelPlay).attr('title', c.labelPlay);
                        slideInfo=false; f.autoPause();
                        f.getClip().unbind('mouseenter mouseleave');
                        view.unbind('mouseenter mouseleave');
                    }
                },
                autoRestart:function(){clearTimeout(t1);
                    if(currentItem+1>=f.getSizeItem()){currentItem=-1}
                    t1=setTimeout(function(){
                    f.getFocus(currentItem+1); f.moveItem(currentItem+1); f.autoRestart()},c.playDuration);
                    
                },
                autoPause:function(){clearInterval(t1)},
                viewCurrent:function(i){cur.html(c.labelFotoImg+' '+(i+1)+' '+c.labelFromImg+' '+f.getSizeItem())}
            });
            f.getClip().addClass('clipCarouselLoad');
            f.getMoveBlock().addClass('listCarouselLoad');
            f.pushAll();
            o.prepend(fake).prepend(view).prepend(prev).append(next).find('.fotoToolBox').prepend(slide).prepend(cur);
            prev.bind('click', function(){
                next.removeClass('disableArrow');
                f.movePage('minus');
            });
            slide.bind('click', function(e){
                f.slideShow();
                e.preventDefault();
            });
            next.bind('click', function(){
                prev.removeClass('disableArrow');
                f.movePage('plus');
            });
            f.getAllItem().each(function(i){
                fakeList.append($('<a href="#">fake'+i+'</a>')).appendTo(f.getClip()).addClass('fakeLink');
                $(this).bind('click', function(e){
                    f.moveItem(i); f.getFocus(i); e.preventDefault();
                }).find('a').attr('tabIndex', '-1');
            });
            fakeList.children('a').each(function(i){
                $(this).bind('focusin', function(){
                    f.moveItem(i);
                });
            });
            f.moveItem(0);
            f.getClip().bind('keydown', function(e){
                if(e.keyCode==37){f.getFocus(currentItem-1); f.moveItem(currentItem-1); return false}
                if(e.keyCode==39){f.getFocus(currentItem+1); f.moveItem(currentItem+1); return false}
            });
        })
    };
    //
    $.fn.tooltipDh = function(params){
        var conf = $.extend({
            effect:'toggle',
            fadeOutSpeed:'normal',
            predelay:0,
            delay:30,
            opacity:1,
            tip:0,
            position:['center', 'right'],
            offset:[0, 0],
            relative:!1,
            cancelDefault:!0,
            events:{
                def:'click,click',
                input:'focus,blur',
                widget:'focus mouseenter,blur mouseleave',
                tooltip:''
            },
            layout:'<div/>',
            tipClass:'view-tip',
            externalHtml:'<div class="itemArrow"><span /></div>',
            innerClose:'<span class="itemClose" />'
        }, params);
        return this.each(function(){
            var b = {
                toggle:[function(a){
                    var b = this.getConf(),
                        c = this.getTip(),
                        d = b.opacity;
                    d < 1 && c.css({
                        opacity: d
                    }), c.show(), a.call()
                }, function (a) {
                    this.getTip().hide(), a.call()
                }],
                fade:[function(a){
                    var b = this.getConf();
                    this.getTip().fadeTo(b.fadeInSpeed, b.opacity, a)
                }, function (a) {
                    this.getTip().fadeOut(this.getConf().fadeOutSpeed, a)
                }]
            };
            var f = this, d = $(this), e = conf,
                g = d.add(f), v=0, c=$(e.innerClose),
                h, i = 0,
                j = 0,
                k = d.attr('title'),
                l = d.attr('href'),
                m = b[e.effect],
                n, o = d.is(':input'),
                p = o && d.is(':checkbox, :radio, select, :button, :submit'),
                q = d.attr('type'),
                r = e.events[q] || e.events[o ? p ? 'widget' : 'input' : 'def'];
            if (!m) throw 'Nonexistent effect \'' + e.effect + '\'';
            r = r.split(/,\s*/);
            if (r.length != 2) throw 'Tooltip: bad events configuration for ' + q;
            if(r[0]=='click' && r[1]=='click'){
                d.bind('click', function(a){
                    if(v){f.hide(a);v=0}else{f.show(a);v=1}
                    return false;
                })
            }else{
                d.bind(r[0], function(a){
                    clearTimeout(i), e.predelay ? j = setTimeout(function(){
                        f.show(a)
                    }, e.predelay) : f.show(a)
                }).bind(r[1], function (a) {
                    clearTimeout(j), e.delay ? i = setTimeout(function(){
                        f.hide(a)
                    }, e.delay) : f.hide(a)
                })
            };
            k && e.cancelDefault && (d.removeAttr('title'), d.data('title', k));
            if(l && l!='#'){(l=$(l), $(l).remove())}
            $.extend(f, {
                setPosition:function(b, c, d){
                    var e = d.relative ? b.position().top : b.offset().top,
                        f = d.relative ? b.position().left : b.offset().left,
                        g = d.position[0];
                    e -= c.outerHeight() - d.offset[0], f += b.outerWidth() + d.offset[1], /iPad/i.test(navigator.userAgent) && (e -= a(window).scrollTop());
                    var h = c.outerHeight() + b.outerHeight();
                    g == 'center' && (e += h / 2), g == 'bottom' && (e += h), g = d.position[1];
                    var i = c.outerWidth() + b.outerWidth();
                    g == 'center' && (f -= i / 2), g == 'left' && (f -= i);
                    return {
                        top: e,
                        left: f
                    }
                },
                close:function(z){
                    c.bind('click', function(a){f.hide(a);v=0;});
                },
                show:function(b){
                    if(!h){
                        l ? h = $(e.layout).append(e.externalHtml).append(c).addClass(e.tipClass).appendTo(document.body).hide().append(l) : e.tip ? h = $(e.tip).eq(0) : k ? h = $(e.layout).append(e.externalHtml).addClass(e.tipClass).appendTo(document.body).hide().append(k) : (h = d.next(), h.length || (h = d.parent().next()));
                        if(!h.length) throw 'Cannot find tooltip for ' + d
                    }
                    if(f.isShown()) return f;
                    
                    h.stop(!0, !0);
                    var o = f.setPosition(d, h, e);
                    e.tip && h.html(d.data('title')), b = b || a.Event(), b.type = 'onBeforeShow', g.trigger(b, [o]);
                    if (b.isDefaultPrevented()) return f;
                    o = f.setPosition(d, h, e), h.css({
                        position: 'absolute',
                        top: o.top,
                        left: o.left
                    }), n = !0, m[0].call(f, function () {
                        b.type = 'onShow', n = 'full', g.trigger(b)
                    });
                    var p = e.events.tooltip.split(/,\s*/);
                    h.data('__set') || (h.bind(p[0], function(){
                        clearTimeout(i), clearTimeout(j)
                    }), p[1] && !d.is('input:not(:checkbox, :radio), textarea') && h.bind(p[1], function(a){
                        a.relatedTarget != d[0] && d.trigger(r[1].split(' ')[0])
                    }), h.data('__set', !0));
                    if(c.length){f.close(b)}
                    return f
                },
                hide:function(c){
                    if(!h || !f.isShown()) return f;
                    c = c || a.Event(), c.type = 'onBeforeHide', g.trigger(c);
                    if(!c.isDefaultPrevented()){
                        n = !1, b[e.effect][1].call(f, function(){
                            c.type = 'onHide', g.trigger(c)
                        });
                        return f
                    }
                },
                isShown:function(a){return a ? n == 'full' : n},
                getConf:function(){return e},
                getTip:function(){return h},
                getTrigger:function(){return d}
            }),
            $.each('onHide,onBeforeShow,onShow,onBeforeHide'.split(','), function(b, c){
                $.isFunction(e[c]) && $(f).bind(c, e[c]), f[c] = function(b){
                    b && $(f).bind(c, b);
                    return f
                }
            })
        })
    }
    //
    $.fn.carouselSmallDh = function(params){
        var conf = $.extend({
            clipBlock:'.carouselCnt',
            moveBlock:'ul.listSmall',
            itemBlock:'li',
            listClass:'carouselNbr',
            playDuration:5000,
            playBlock:'unitSlide',
            labelPlay:'',
            labelStop:''
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),
                f=this,
                tO=null,
                blockNav=$('<div />').addClass(c.listClass),
                fake=$('<div />').addClass('unitFake'), slideInfo=false,
                //fakeTab=$('<a href="">fake</a>').addClass('linkFake'),
                listNav, slide=$('<a href="#" title="'+c.labelPlay+'">'+c.labelPlay+'</a>').addClass(c.playBlock),
                currentItem=0;
            $.extend(f,{
                getClip:function(){return o.find(c.clipBlock)},
                getMoveBlock:function(){return f.getClip().find(c.moveBlock)},
                getWidthItem:function(){return f.getMoveBlock().find(c.itemBlock).outerWidth(true)},
                getAllItem:function(){return f.getMoveBlock().children(c.itemBlock)},
                getSizeItem:function(){return f.getAllItem().size()},
                moveItem:function(){
                    if(currentItem>=f.getSizeItem()){currentItem=0}
                    if(currentItem<0){currentItem=f.getSizeItem()-1}
                    fake.animate({opacity:1}, 100, function(){
                        f.getMoveBlock().css({left:-currentItem*f.getWidthItem()});
                        fake.removeClass('hideElement');
                        listNav.removeClass('itemActive').eq(currentItem).addClass('itemActive');
                        fake.animate({opacity:0}, 300, function(){fake.addClass('hideElement')});
                    });
                },
                listBlock:function(){
                    f.getAllItem().each(function(i){
                        blockNav.append($('<li />').html('<a href="#">'+(i+1)+'</a>'));
                    }).find('a').attr('tabIndex', '-1');;
                    blockNav.wrapInner('<ul />').prepend(slide);
                    listNav=blockNav.find('li');
                    listNav.each(function(index){
                        $(this).bind('focusin', function(e){currentItem=index; f.moveItem();
                        }).bind('click', function(e){e.preventDefault()})
                    })
                    o.append(blockNav);
                },
                slideShow:function(){
                    if(slideInfo==false){
                        slide.addClass('startSlide').html(c.labelStop).attr('title', c.labelStop);
                        slideInfo=true; f.autoRestart();
                        o.bind('mouseenter focusin', function(){f.autoPause()}).bind('mouseleave focusout',function(){f.autoRestart()});
                    }else{
                        slide.removeClass('startSlide').html(c.labelPlay).attr('title', c.labelPlay);
                        slideInfo=false; f.autoPause();
                        o.unbind('mouseenter mouseleave focusin focusout');
                    }
                },
                autoRestart:function(){clearTimeout(tO); tO=setTimeout(function(){++currentItem; f.moveItem(); f.autoRestart()},c.playDuration)},
                autoPause:function(){clearInterval(tO)}
            });
            if(f.getClip().html()!=null){
                f.getClip().addClass('clipScriptLoad');
                f.getClip().prepend(fake);
                f.listBlock(); f.moveItem();
                listNav.bind('keydown', function(e){
                    if(e.keyCode==37){f.moveItem(--currentItem); listNav.children('a').eq(currentItem).focus(); return false}
                    if(e.keyCode==39){f.moveItem(++currentItem); listNav.children('a').eq(currentItem).focus(); return false}
                    if(e.keyCode==13){
                        if(f.getAllItem().eq(currentItem).find('a')!=null){
                            document.location=f.getAllItem().eq(currentItem).find('a').attr('href'); return false
                        }
                    }
                });
                slide.bind('click', function(e){f.slideShow(); e.preventDefault()});
                f.slideShow();
            }
        })
    };
    //
    $.fn.carouselDh = function(params){
        var conf = $.extend({
            clipBlock:'.clipCarousel',
            moveBlock:'ul',
            itemBlock:'li',
            sizePage:5,
            prevPage:'itemArrowLeft',
            nextPage:'itemArrowRight',
            pageBlock:'.page',
            noScrollSize:120,
            sizeScroll:3,
            labelPrev:'',
            labelNext:''
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),
                f=this,
                prev=$('<a />').addClass(c.prevPage).attr('title', c.labelPrev).html(c.labelPrev),
                next=$('<a />').addClass(c.nextPage).attr('title', c.labelNext).html(c.labelNext),
                currentItem=0,
                currentPage=0,
                positionMoveBlock=0,
                timeOut=null, pagePosition=null;
            $.extend(f,{
                getClip:function(){return o.find(c.clipBlock)},
                getMoveBlock:function(){return o.find(c.moveBlock)},
                getWidthItem:function(){return f.getMoveBlock().find(c.itemBlock).outerWidth(true)},
                getSizeItem:function(){return f.getAllItem().size()},
                getAllItem:function(){return f.getMoveBlock().children(c.itemBlock)},
                getSizePrevScroll:function(){return f.getClip().width()/2-c.noScrollSize+f.getClip().offset().left},
                getSizeNextScroll:function(){return f.getClip().width()/2+c.noScrollSize+f.getClip().offset().left},
                getLastPage:function(){return (Math.ceil((f.getSizeItem()-c.sizePage)))*f.getWidthItem()},
                movePage:function(pageIn){
                    currentItem=pageIn*c.sizePage;
                    if(currentItem<=0){currentItem=0; currentPage=0; positionMoveBlock=0; prev.addClass('disableArrow')}
                    if(currentItem>=f.getSizeItem()-c.sizePage){
                        currentItem=f.getSizeItem()-c.sizePage;
                        currentPage=Math.floor(currentItem/c.sizePage);
                        next.addClass('disableArrow');
                    }
                    f.getMoveBlock().animate({left:-currentItem*f.getWidthItem()});
                    positionMoveBlock=currentItem*f.getWidthItem();
                },

                hoverTime:function(){
                    timeOut=setInterval(function(){
                        if(pagePosition>f.getSizeNextScroll()){
                            if(!(f.getMoveBlock().position().left <= -f.getLastPage())){positionMoveBlock+=c.sizeScroll; prev.removeClass('disableArrow')}
                            else{positionMoveBlock=f.getLastPage(); next.addClass('disableArrow')}
                        }
                        if(pagePosition<f.getSizePrevScroll()){
                            if(!(f.getMoveBlock().position().left >= 0)){positionMoveBlock-=c.sizeScroll; next.removeClass('disableArrow')}
                            else{positionMoveBlock=0; prev.addClass('disableArrow')}
                            
                        }
                        f.getMoveBlock().css({left:-positionMoveBlock});
                        currentItem=Math.floor(positionMoveBlock/f.getWidthItem());
                        currentPage=Math.floor(currentItem/c.sizePage);
                    }, 2)
                },
                opacityItem:function(){
                    f.getMoveBlock().find('img').each(function(){
                        $(this).css({opacity:0.7})
                            .hover(function(){$(this).css({opacity:1})},function(){$(this).css({opacity:0.7})});
                    })
                },
                onStart:function(){
                    var s;
                    f.getClip().addClass('clipScriptLoad');
                    f.getMoveBlock().addClass('listScriptLoad');
                    f.opacityItem();
                    if(f.getWidthItem()*f.getAllItem().length>f.getClip().width()){
                        o.prepend(prev);
                        o.append(next);
                        if(f.getAllItem().length%2){
                            s=(f.getWidthItem()*(f.getAllItem().length+1))/2-(f.getClip().width()/2);
                        }else{
                            s=(f.getWidthItem()*f.getAllItem().length)/2-(f.getClip().width()/2);
                        }
                        f.getMoveBlock().css({left:-s});
                        positionMoveBlock=s;
                        f.bindHoverScroll();
                        prev.bind('click', function(){
                            next.removeClass('disableArrow');
                            f.movePage(--currentPage);
                        });
                        next.bind('click', function(){
                            prev.removeClass('disableArrow');
                            f.movePage(++currentPage);
                        });
                    }else{
                        s=0;
                        f.getMoveBlock().css({left:-s});
                    }
                },
                bindHoverScroll:function(){
                    f.getClip().bind('mouseenter',function(){
                        f.hoverTime();
                    }).bind('mouseleave',function(){clearInterval(timeOut)}).bind('mousemove', function(e){pagePosition=e.pageX});
                }
            });
            f.onStart();
        })
    }
})(jQuery);
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);
//(function(){var e=typeof jQuery=="function";var i={width:"100%",height:"100%",allowfullscreen:true,allowscriptaccess:"always",quality:"high",version:null,onFail:null,expressInstall:null,w3c:false,cachebusting:false};if(e){jQuery.tools=jQuery.tools||{};jQuery.tools.flashembed={version:"1.0.4",conf:i}}function j(){if(c.done){return false}var l=document;if(l&&l.getElementsByTagName&&l.getElementById&&l.body){clearInterval(c.timer);c.timer=null;for(var k=0;k<c.ready.length;k++){c.ready[k].call()}c.ready=null;c.done=true}}var c=e?jQuery:function(k){if(c.done){return k()}if(c.timer){c.ready.push(k)}else{c.ready=[k];c.timer=setInterval(j,13)}};function f(l,k){if(k){for(key in k){if(k.hasOwnProperty(key)){l[key]=k[key]}}}return l}function g(k){switch(h(k)){case"string":k=k.replace(new RegExp('(["\\\\])',"g"),"\\$1");k=k.replace(/^\s?(\d+)%/,"$1pct");return'"'+k+'"';case"array":return"["+b(k,function(n){return g(n)}).join(",")+"]";case"function":return'"function()"';case"object":var l=[];for(var m in k){if(k.hasOwnProperty(m)){l.push('"'+m+'":'+g(k[m]))}}return"{"+l.join(",")+"}"}return String(k).replace(/\s/g," ").replace(/\'/g,'"')}function h(l){if(l===null||l===undefined){return false}var k=typeof l;return(k=="object"&&l.push)?"array":k}if(window.attachEvent){window.attachEvent("onbeforeunload",function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){}})}function b(k,n){var m=[];for(var l in k){if(k.hasOwnProperty(l)){m[l]=n(k[l])}}return m}function a(r,t){var q=f({},r);var s=document.all;var n='<object width="'+q.width+'" height="'+q.height+'"';if(s&&!q.id){q.id="_"+(""+Math.random()).substring(9)}if(q.id){n+=' id="'+q.id+'"'}if(q.cachebusting){q.src+=((q.src.indexOf("?")!=-1?"&":"?")+Math.random())}if(q.w3c||!s){n+=' data="'+q.src+'" type="application/x-shockwave-flash"'}else{n+=' classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'}n+=">";if(q.w3c||s){n+='<param name="movie" value="'+q.src+'" />'}q.width=q.height=q.id=q.w3c=q.src=null;for(var l in q){if(q[l]!==null){n+='<param name="'+l+'" value="'+q[l]+'" />'}}var o="";if(t){for(var m in t){if(t[m]!==null){o+=m+"="+(typeof t[m]=="object"?g(t[m]):t[m])+"&"}}o=o.substring(0,o.length-1);n+='<param name="flashvars" value=\''+o+"' />"}n+="</object>";return n}function d(m,p,l){var k=flashembed.getVersion();f(this,{getContainer:function(){return m},getConf:function(){return p},getVersion:function(){return k},getFlashvars:function(){return l},getApi:function(){return m.firstChild},getHTML:function(){return a(p,l)}});var q=p.version;var r=p.expressInstall;var o=!q||flashembed.isSupported(q);if(o){p.onFail=p.version=p.expressInstall=null;m.innerHTML=a(p,l)}else{if(q&&r&&flashembed.isSupported([6,65])){f(p,{src:r});l={MMredirectURL:location.href,MMplayerType:"PlugIn",MMdoctitle:document.title};m.innerHTML=a(p,l)}else{if(m.innerHTML.replace(/\s/g,"")!==""){}else{m.innerHTML="<h2>Flash version "+q+" or greater is required</h2><h3>"+(k[0]>0?"Your version is "+k:"You have no flash plugin installed")+"</h3>"+(m.tagName=="A"?"<p>Click here to download latest version</p>":"<p>Download latest version from <a href='http://www.adobe.com/go/getflashplayer'>here</a></p>");if(m.tagName=="A"){m.onclick=function(){location.href="http://www.adobe.com/go/getflashplayer"}}}}}if(!o&&p.onFail){var n=p.onFail.call(this);if(typeof n=="string"){m.innerHTML=n}}if(document.all){window[p.id]=document.getElementById(p.id)}}window.flashembed=function(l,m,k){if(typeof l=="string"){var n=document.getElementById(l);if(n){l=n}else{c(function(){flashembed(l,m,k)});return}}if(!l){return}if(typeof m=="string"){m={src:m}}var o=f({},i);f(o,m);return new d(l,o,k)};f(window.flashembed,{getVersion:function(){var m=[0,0];if(navigator.plugins&&typeof navigator.plugins["Shockwave Flash"]=="object"){var l=navigator.plugins["Shockwave Flash"].description;if(typeof l!="undefined"){l=l.replace(/^.*\s+(\S+\s+\S+$)/,"$1");var n=parseInt(l.replace(/^(.*)\..*$/,"$1"),10);var r=/r/.test(l)?parseInt(l.replace(/^.*r(.*)$/,"$1"),10):0;m=[n,r]}}else{if(window.ActiveXObject){try{var p=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7")}catch(q){try{p=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");m=[6,0];p.AllowScriptAccess="always"}catch(k){if(m[0]==6){return m}}try{p=new ActiveXObject("ShockwaveFlash.ShockwaveFlash")}catch(o){}}if(typeof p=="object"){l=p.GetVariable("$version");if(typeof l!="undefined"){l=l.replace(/^\S+\s+(.*)$/,"$1").split(",");m=[parseInt(l[0],10),parseInt(l[2],10)]}}}}return m},isSupported:function(k){var m=flashembed.getVersion();var l=(m[0]>k[0])||(m[0]==k[0]&&m[1]>=k[1]);return l},domReady:c,asString:g,getHTML:a});if(e){jQuery.fn.flashembed=function(l,k){var m=null;this.each(function(){m=flashembed(this,l,k)});return l.api===false?this:m}}})();

/*! jq.function.js */
$(document).ready(function(){
    // dropdown lang
    $('.itemLang ul').dropDownDh({activeClass:'activeItem'});
    $('.unitListHeader li.itemSize').cssDh();
    // top form text
    $('.unitFormHeader form').seacrhDh();
    // columns & dropdown menu
    $('.assemblyMenu .unitSubMenu').columnDh();
    $('.assemblyMenu .unitSubMenu').dropDownDh({activeClass:'itemHover'});
    // transparent bottom block news
    //$('.stationMediumImg ul').opacityBlockDh({opacityValue:0.9});
    // transparent logo in news
    $('.stationMediumImg .unitLogo').transparentBlockDh();
    
    //$('.searchFAQ').accordDh();
    if ($('.searchFAQ').length){
        $('.searchFAQ dd div').hide();
        $('.searchFAQ dt select').live('change', function(obj){
            $('.searchFAQ dd, .searchFAQ dd div').hide();
            $(obj.target).parents('dt').next().show();
            $('#'+$(obj.target).val()).show();
        });
        $('.searchFAQ dt a').live('click', function(obj){
            if ($($(obj.target).parents('dt').next()).attr('class') == 'visible'){
                $(obj.target).parents('dt').next().hide();
                $('#'+$(obj.target).next().val()).hide();
                $(obj.target).parents('dt').next().removeAttr('class');
            } else {
                $('.searchFAQ dd, .searchFAQ dd div').hide();
                $(obj.target).parents('dt').next().show();
                $('#'+$(obj.target).next().val()).show();
                $(obj.target).parents('dt').next().attr('class', 'visible');
            }
        });
    };
    
    $('.assemblySub').olDh();
    $('.artikelUsefulInfo .itemLink a').popupDh({contentBlock:'#tip_redactie', positionBlock:['top', -335]});
    $('.pageToolBox a[href="#stuur_artikel"], .fotoToolBox a[href="#stuur_artikel"]').popupDh({contentBlock:'#stuur_artikel', positionBlock:['top', -25]});
    $('.pageToolBox a[href="#toevoegen_aan"]').popupDh({contentBlock:'#toevoegen_aan', positionBlock:['bottom', -40]});
    $('.fotoToolBox a[href="#toevoegen_aan"]').popupDh({contentBlock:'#toevoegen_aan', positionBlock:['bottom', -40]});
    $('.artikelUsefulInfo').infoFormDh();
    $('.itemHeaderLogo').slideLogoDh();
    $('.sideCallCentre a').callCenterDh();
    $('a.openPopup').openPopupDh();
    $('.calendarDayCell .event div').tooltipDh();
    //make textarea empty onclick, focus
    $(".stationForm textarea, .hideForm textarea").one('click focus', function(){
        $(this).val('');
    });
    $('.toolBoxPrint').printLinkDh();
});
if(jQuery) (function($){
    $.fn.slideLogoDh = function(params){
        var conf = $.extend({
            itemBlock:'li',
            moveBlock:'ul',
            startItem:0,
            playDuration:5000,
            fadeTime:500
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),f=this,i=c.startItem,t=null;
            $.extend(f,{
                getSize:function(){return o.find(c.itemBlock).size()},
                getMoveBlock:function(){return o.find(c.moveBlock)},
                getWidthItem:function(){return f.getMoveBlock().find(c.itemBlock).outerWidth(true)},
                moveItem:function(k){
                    f.getMoveBlock().animate({opacity:0}, c.fadeTime,
                        function(){
                            if(i>=f.getSize()){
                                f.getMoveBlock().css({left:0}); i=0;
                            }else{
                                f.getMoveBlock().css({left:-(i*f.getWidthItem()-1)})
                            }
                            f.getMoveBlock().animate({opacity:1}, c.fadeTime);
                        }
                    );
                },
                autoRestart:function(){
                    clearTimeout(t);
                    t=setTimeout(function(){
                        f.moveItem(++i);
                        f.autoRestart();
                    },c.playDuration);
                },
                autoPause:function(){
                    clearInterval(t);
                }
            });
            if((f.getSize()>1) && ((c.fadeTime*2)<c.playDuration)){
                f.getMoveBlock().addClass('listScriptLoad');
                f.autoRestart();
                o.hover(
                    function(){f.autoPause()},function(){f.autoRestart()}
                )
            }
        })
    };
    $.fn.callCenterDh = function(params){
        var conf = $.extend({
            hideClass:'hideElement'
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),t=o.attr('href');
            if($(t)){
                $(t).addClass(c.hideClass);
                o.bind('click', function(){
                    if($(t).hasClass(c.hideClass)){$(t).removeClass(c.hideClass); return false}
                    else{$(t).addClass(c.hideClass); return false}
                })
            }
        })
    };
    $.fn.openPopupDh = function(params){
        var conf = $.extend({
            width:600,
            height:700
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),t=o.attr('href');
            o.bind('click', function(){
                window.open(t, '', 'width='+c.width+',height='+c.height+',scrollbars=1'); return false;
            })
            
        })
    };
    $.fn.printLinkDh = function(params){
        var conf = $.extend({
            parentClass:'hideElement'
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),p=o.parent();
            if(p.hasClass(c.parentClass)){
                p.removeClass(c.parentClass);
                o.bind('click', function(){window.print(); return false;});
            }
        })
    };
    $.fn.infoFormDh = function(params){
        var conf = $.extend({
            headerBlock:'input:radio',
            contentBlock:'fieldset'
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),a=o.find(c.headerBlock),b=o.find(c.contentBlock);
            if((b != null) && (a != null)){
                b.addClass('hideElement');
            }
            a.one('click', function(){
                b.removeClass('hideElement');
            })
        })
    };
    $.fn.olDh = function(params){
        var conf = $.extend({
            numberClass:'itemNumber'
        }, params);
        return this.each(function(){
            var c=conf,o=$('ol', this),f=this;
            $.extend(f,{
                addElement:function(e,c,p,t){
                    var i=document.createElement(e);
                    i=$(i).addClass(c);
                    if(t){p.append(i)}else{p.prepend(i)}
                    return i;
                }
            });
            o.each(function(){
                $(this).addClass('noListType');
                $(this).children().each(function(i){
                    i++;
                    if($(this).find('.'+c.numberClass).html() != null){
                        $(this).find('.'+c.numberClass).append(i+'.');
                    }else{
                        f.addElement('span',c.numberClass,$(this)).html(i+'.');
                        $('li', this).each(function(){
                            f.addElement('span',c.numberClass,$(this)).html(i+'.');
                        })
                    };
                });
            })
        })
    };
    $.fn.cssDh = function(){
        return this.each(function(){
            var o=$(this),l=$(this).find('a'),a;
            o.addClass('showElement');
            l.each(function(i){
                if($(this).hasClass('itemActive')){a=i}
                $(this).click(function(e){
                    $('link[title=changeFont]').attr('href', $(this).attr('rel'));
                    $(this).addClass('itemActive');
                    l.eq(a).removeClass('itemActive');
                    a=i;
                    e.preventDefault();
                })
            })
        })
    };
    $.fn.popupDh = function(params){
        var conf = $.extend({
            contentBlock:'div',
            linkClose:'.closePopup',
            activeClass:'itemActive',
            positionBlock:['top', 'left']
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),f=this,w=$(document).find(c.contentBlock),v=null,tp,lp,bm,bt,bb;
            $.extend(f,{
                bindClick:function(e){
                    if(v==false){f.showItem(e)}else if(v==true){f.hideItem(e)}
                },
                loadItem:function(){
                    w.appendTo('body');
                    w.addClass('hidePopup');
                    bt=$('<div />').addClass('btmAngle').prependTo(w).transparentBlockDh();
                    bm=$('<div />').addClass('popupBg').prependTo(w).transparentBlockDh();
                    bb=$('<div />').addClass('topAngle').prependTo(w).transparentBlockDh();
                    f.bindClose();
                    v=false;
                },
                showItem:function(){
                    f.positionItem();
                    v=true;
                    o.addClass(c.activeClass);
                },
                hideItem:function(){
                    v=false;
                    o.removeClass(c.activeClass);
                    w.removeAttr('style');
                },
                bindClose:function(){
                    w.find(c.linkClose).bind('click', function(e){f.hideItem(e);e.preventDefault();})
                },
                positionItem:function(){
                    bm.css({height:w.height()-bt.height()-bb.height()});
                    if(c.positionBlock[0]=='top'){
                        tp=o.offset().top-w.outerHeight();
                    }else if(c.positionBlock[0]=='bottom'){
                        tp=o.offset().top+o.outerHeight();
                    }else{
                        tp=o.offset().top+c.positionBlock[0];
                    }
                    if(c.positionBlock[1]=='left'){
                        lp=o.offset().left-w.outerWidth();
                    }else if(c.positionBlock[1]=='right'){
                        lp=o.offset().left+o.outerWidth();
                    }else{
                        lp=o.offset().left+c.positionBlock[1];
                    }
                    w.css({top:Math.round(tp), left:Math.round(lp)});
                }
            });
            f.loadItem();
            o.bind('click', function(e){
                f.bindClick();
                e.preventDefault();
            });
        })
    };
    $.fn.accordDh = function(params){
        var conf = $.extend({
            headerBlock:'dt',
            contentBlock:'dd',
            openClass:'active',
            openItem:-1
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),f=this,h=o.children(c.headerBlock),d=-1;
            $.extend(f,{
                openItem:function(m){
                    m.next().css({display:'block'});
                    m.addClass(c.openClass);
                },
                closeItem:function(m){
                    m.next().css({display:'none'});
                    m.removeClass(c.openClass);
                }
            })
            h.each(function(i){
                var v=$(this);
                if(i==c.openItem){
                    f.openItem(v);
                    d=i;
                }
                if(v.next().is(c.contentBlock)){
                    v.bind('click', function(e){
                        if(d!=i){
                            f.openItem(v);
                            f.closeItem($(h[d]));
                            d=i;
                        }else{
                            f.closeItem(v);
                            d=-1;
                        }
                        e.preventDefault();
                    })
                }
            })
            
        })
    };
    $.fn.transparentBlockDh = function(){
        return this.each(function(){
            var o=$(this),f=this,b,s='',c='',n=navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && (navigator.appVersion.indexOf("MSIE 5.5") != -1 || navigator.appVersion.indexOf("MSIE 6.0") != -1);
            if(n==true && $.browser.msie){
                if(f.tagName.toLowerCase()=='img' && o.attr('src').indexOf(".png")!=-1){
                    o.attr('width',o.width());
                    o.attr('height',o.height());
                    c = (o.attr('id')) ? 'id="' + o.attr('id') + '" ' : '';
                    c += (o.attr('class')) ? 'class="' + o.attr('class') + '" ' : '';
                    c += (o.attr('title')) ? 'title="' + o.attr('title') + '" ' : '';
                    c += (o.attr('alt')) ? 'alt="' + o.attr('alt') + '" ' : '';
                    s += '<span '+ c + 'style="position:relative;display:inline-block;background:transparent;';
                    s += 'width:' + o.width() + 'px;' + 'height:' + o.height() + 'px;';
                    s += 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader' + '(src=\'' + o.attr('src') + '\', sizingMethod=\'scale\');"></span>';                 
                    o.after(s);
                    o.remove();
                }else{
                    b=o.css('backgroundImage');
                    if(b.indexOf(".png")!=-1){
                        b= b.split('url("')[1].split('")')[0];
                        o.css('background-image', 'none');
                        o.get(0).runtimeStyle.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + b + "',sizingMethod='scale')";
                    }
                }
            }
        })
    };
    //
    $.fn.seacrhDh = function(params){
        var conf = $.extend({
            textInput:'input:text',
            valueDef:'span.valueText'
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),i=o.find(c.textInput),t=o.find(c.valueDef).html();
            if(t==null){t='Stel uw vraag'};
            if(i.val()==''){i.val(t)};
            o.submit(function(){
                if((i.val()==t)||(i.val()=='')){
                    return false;
                }
            });
            i.blur(function(){
                if(i.val()==''){
                    i.val(t);
                }
            });
            i.focus(function(){
                if(i.val()==t){
                    i.val('');
                }
            });
        })
    };
    //
    /*$.fn.opacityBlockDh = function(params){
        var conf = $.extend({
            opacityValue:0.9
        }, params);
        return this.each(function(){
            var o=$(this),f=this,c=conf,b,g=o.css('backgroundColor');
            $.extend(f,{
                addElement:function(e,c,p,t){
                    var i=document.createElement(e);
                    i=$(i).addClass(c);
                    if(t){p.append(i)}else{p.before(i)}
                    return i;
                },
                addBackground:function(){
                    b=f.addElement('div','',o,false);
                    b.css({
                        position:'absolute',
                        zIndex:1,height:o.outerHeight(),
                        width:o.outerWidth(),
                        top:o.position().top,
                        left:o.position().left,
                        opacity:c.opacityValue,
                        backgroundColor:g
                    });
                }
            });
            o.css({backgroundColor:'transparent'});
            f.addBackground();
        })
    };*/
    $.fn.columnDh = function(params){
        var conf = $.extend({
            targetItem:'li',
            columnSize:2
        }, params);
        return this.each(function(){
            var o=$(this),c=conf,e={};
            e.l = o.children().size()-1;
            e.s = 0;
            $.each(o.children(), function(i) { if($(this).is(c.targetItem)) {e.s++} });
            e.m = Math.ceil(e.s/c.columnSize);
            e.t = e.m;
            e.g = o[0].tagName.toLowerCase();
            e.k = 0;
            e.n = $('<div class="'+o.attr('class')+'"></div>');
            e.i = $('<'+e.g+'></'+e.g+'>');
            $.each(o.children(), function(i) {
                if((e.k == e.t) && $(this).is(c.targetItem)) {
                    e.t += e.m;
                    $(e.n).append(e.i).insertBefore(o);
                    e.i = $('<'+e.g+'></'+e.g+'>');
                }
                if($(this).is(c.targetItem)) e.k++;
                e.i.append($(this));
                if(i == e.l) { $(e.n).append(e.i).insertBefore(o)}
            });
            $(this).replaceWith(o.html());
        });
    };
    $.fn.dropDownDh = function(params){
        var conf = $.extend({
            bindAction:'hover',
            activeClass:'active'
        }, params);
        return this.each(function(){
            var o=$(this),c=conf,p=o.parent().eq(0),m=o.parent().children().eq(0);
            p.hoverIntent({
                timeout: 150,
                over: function(){
                    if(c.bindAction == 'hover'){
                        o.css('opacity', 0).css('visibility', 'visible').animate({opacity:1}, 100);
                        p.addClass(c.activeClass);
                    }
                },
                out: function(){
                    o.animate({opacity:0}, 100, function(){
                        o.css('visibility', 'hidden');
                        p.children().eq(0).removeClass('active');
                        p.removeClass(c.activeClass);
                    });
                    
                }
            });
            if(c.bindAction == 'click'){
                m.click(function(){
                    if(o.css('visibility') == 'hidden') {
                        o.css('opacity', 0).css('visibility', 'visible').animate({opacity:1}, 100);
                        m.addClass(c.activeClass);
                    } else {
                        o.animate({opacity:0}, 100, function(){
                            o.css('visibility', 'hidden');
                            m.removeClass(c.activeClass);
                        });
                    }
                    return false;
                });
            }
        });
    };
    //
    $.fn.accordRisDh = function(params){
        var conf = $.extend({
            headerBlock:'dt',
            contentBlock:'dd',
            openClass:'active',
            nameId:'accord-',
            textOpen:'Alles uitklappen',
            textClose:'Alles inklappen'
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),f=this,a=0,s=0,sa=new Array(),l=$('<a href="#"></a>');
            $.extend(f,{
                openItem:function(hI,bI,e){
                    if(e=='open'){hI.addClass('itemOpen').removeClass('itemClose');bI.slideDown();s+=1;
                    }else{hI.addClass('itemClose').removeClass('itemOpen');bI.slideUp();s-=1;
                    }
                    if(sa.length==s){f.linkChange(l,c.textClose)}else{f.linkChange(l,c.textOpen)}
                },
                openAll:function(){
                    if(sa.length==s){
                        s=sa.length;
                        $.each(sa,function(){f.openItem($(this).children('dt'),$(this).children('dd'),'close');});
                    }else{
                        s=0;
                        $.each(sa,function(){f.openItem($(this).children('dt'),$(this).children('dd'),'open');});
                    }
                },
                findBlock:function(){return o.children('dl')},
                linkChange:function(lnk, txt){lnk.text(txt).attr('title', txt)},
                openChange:function(){
                    f.findBlock().each(function(){
                        if($(this).children('dd').html()!=''){sa.push($(this))}
                    });
                    f.linkChange(l,c.textOpen);
                    $('p.itemSort', o).append(l);
                    l.bind('click', function(){f.openAll();return false;})
                }
            });
            f.findBlock().each(function(i){
                var elBlock=$(this), elHead=elBlock.children('dt'), elBody=elBlock.children('dd');
                elBody.css({display:'none'});
                if(elBody.html()!=''){
                    var elLink=$('<a href="#'+c.nameId+i+'"></a>').html(elHead.html());
                    elHead.empty().append(elLink); elBody.attr('id', c.nameId+i);
                    elHead.addClass('itemClose');
                    elHead.bind('click', function(e){
                        if(elHead.hasClass('itemClose')){
                            f.openItem(elHead,elBody,'open');
                        }else{
                            f.openItem(elHead,elBody,'close');
                        }
                        e.preventDefault()
                    }).bind('mouseenter focusin', function(){elHead.addClass('itemHover')
                    }).bind('mouseleave focusout', function(){elHead.removeClass('itemHover')})
                }
            });
            f.openChange();
        })
    };
})(jQuery);

/*! lib.js */
var img;
var img_mo;
var img_cl;
img = new Array();
img_mo = new Array();
img_cl = new Array();

function initMo(uniqueid, origImgSrc, overImgSrc, clickImgSrc) {
  if (origImgSrc != '') {
    img[uniqueid] = new Image();
    img[uniqueid].src = origImgSrc;
  }
  if (overImgSrc != '') {
    img_mo[uniqueid] = new Image();
    img_mo[uniqueid].src = overImgSrc;
  }
  if (clickImgSrc != '') {
    img_cl[uniqueid] = new Image();
    img_cl[uniqueid].src = clickImgSrc;
  }
};

function mov(uniqueid) {
  if (img_mo[uniqueid]) {
    if (typeof document[uniqueid] != 'undefined') {
        document[uniqueid].src = img_mo[uniqueid].src;
    } else {
        obj = document.getElementById(uniqueid);
        if (typeof obj != 'undefined') {
            obj.src = img_mo[uniqueid].src;
        }
    }
  }
};

function mou(uniqueid) {
  if(img[uniqueid]) {
    if (typeof document[uniqueid] != 'undefined') {
        document[uniqueid].src = img[uniqueid].src;
    } else {
        obj = document.getElementById(uniqueid);
        if (typeof obj != 'undefined') {
            obj.src = img[uniqueid].src;
        }
    }
  }
};

function md(uniqueid) {
  if (img_cl[uniqueid]) {
    if (typeof document[uniqueid] != 'undefined') {
        document[uniqueid].src = img_cl[uniqueid].src;
    } else {
        obj = document.getElementById(uniqueid);
        if (typeof obj != 'undefined') {
            obj.src = img_cl[uniqueid].src;
        }
    }
  }
};

function setCookie(name, value, expire) {
  if (expire == '') {
    document.cookie = name + '=' + escape(value) + '; path=/';
  } else {
    var expires = new Date();
    expires.setTime(expires.getTime() + expire);
    document.cookie = name + '=' + escape(value) + ((expire == null) ? '' : ('; expires=' + expires.toGMTString())) + '; path=/';
  }
};

function getCookie(name) {
   var search = name + "=";
   var val = "";
   var offset,end;
   
   if(document.cookie.length > 0) { // if there are any cookies
      offset = document.cookie.indexOf(search) 
      if(offset != -1) { // if cookie exists 
         offset += search.length;
         // set index of beginning of value
         end = document.cookie.indexOf(";", offset) 
         // set index of end of cookie value
         if (end == -1) {
            end = document.cookie.length;
         }
         val = unescape(document.cookie.substring(offset, end));
      } 
   }
   return val;
};

function newSession() {
  if (getCookie('session') == 'set') {
    return false;
  } else {
    setCookie('session', 'set', '');
    if (getCookie('session') == 'set') {
      return true;
    } else {
      return false;
    }
  }
};

function showURL(url, windowname, width, height) {
  var w;
  w = top.open(url, windowname, 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,height=' + height + ',width=' + width);
  if(w==null) {
    return null;
  }
  w.opener = window;
  return w;
};

function addOnLoadListener(func) {
  if (window.addEventListener) {
    window.addEventListener("load", func, true);
  } else if (window.attachEvent) {
    window.attachEvent("onload", func);   
  }
};

/*! readspeaker.js */
function getSelectedHTML() {
    selectedString="";
    var rng=undefined;
    if (window.getSelection) {
        selobj = window.getSelection();
        if (!selobj.isCollapsed) {
            if (selobj.getRangeAt) {
                rng=selobj.getRangeAt(0);
            }
            else {
                rng = document.createRange();
                rng.setStart(selobj.anchorNode,selobj.anchorOffset);
                rng.setEnd(selobj.focusNode,selobj.focusOffset);
            }
            if (rng) {
                DOM = rng.cloneContents();
                object = document.createElement('div');
                object.appendChild(DOM.cloneNode(true));
                selectedString=object.innerHTML;
            }
            else {
                selectedString=selobj;
            }
        }
    }
    else if (document.selection) {
        selobj = document.selection;
        rng = selobj.createRange();
        if (rng && rng.htmlText) {
            selectedString = rng.htmlText;
        }
        else if (rng && rng.text) {
            selectedString = rng.text;
        }
    }
    else if (document.getSelection) {
        selectedString=document.getSelection();
    }
    
    document.getElementById('rs_form').selectedhtml.value = selectedString;
    if (document.getElementById('rs_form').url) {
        if (!document.getElementById('rs_form').url.value) {
            if (window.location.href) {
                document.getElementById('rs_form').url.value=window.location.href;
            }
            else if (document.location.href) {
                document.getElementById('rs_form').url.value=document.location.href;
            }
        }
    }
};

function copyselected() {
    setTimeout("getSelectedHTML()",50);
    return true;
};

function openAndRead() {
    document.getElementById('rs_button').setAttribute("target","rs");
    document.getElementById('rs_form').setAttribute("target","rs");
    var mywindow = window.open("","rs","width=310,height=120,toolbar=0");
    setTimeout("document.getElementById('rs_form').submit();",500);
};

var selectedString="";
document.onmouseup = copyselected;
document.onkeyup = copyselected;

/*! reportabuse.js */
function reportAbuse(postingId, forumName, content, date) {             
    var postingIdField = document.getElementById('posting_postingId');
    var forumNameField = document.getElementById('posting_forumName');
    var contentField = document.getElementById('posting_content');          
    var dateField = document.getElementById('posting_date');
    
    postingIdField.value = 'abuse-'+postingId+'-';
    forumNameField.value = forumName;
    contentField.value = content;
    dateField.value = date;

    var form = document.getElementById('reportAbuse');
    form.submit();
};

/*! userinteraction.js */
function submitRating(contentId,id){
    document.getElementById('vote'+contentId).value=id;
    document.getElementById('votingform'+contentId).submit();
};

/*! toolbox.js */
function callSlideShow(playerLink) {
    var albumId;
    if (document.getElementById("albumId") != null) {
        albumId = document.getElementById("albumId").value;
        window.open(playerLink.replace('replace', document.getElementById("albumId" ).value), "_blank","menubar=1,resizable=1,width=screen.availWidth,height=screen.availHeight");
    } else {
        alert("<wm:text label='page_has_no_album' />");
    }
};
function checkAlbumOnPage() {
    if (document.getElementById("albumId") == null) {
        var aElement = document.getElementById("slideShowTag");
        if (aElement != null) {
            var parentElement = aElement.parentNode;
            parentElement.removeChild(aElement);
        }
    }
};

/*! qgoclick.js */
var xmlHttp;
var noMatchLink;

function doQgoClick(handleId, questionId, answerId, alias, siteRef, keywordsession) {
    xmlHttp = GetXmlHttpObject();

    if (xmlHttp == null) {
        // unable to register the click since this browser does not support http request.
        return;
    } 

    var url = "/web/wcbservlet/com.gxwebmanager.gxpublic.qgoelement.servlet";
    url = url + "?clicktype=qgo";
    url = url + "&handleid=" + handleId;
    url = url + "&questionid=" + questionId;
    url = url + "&answerid=" + answerId;
    url = url + "&alias=" + alias;
    url = url + "&siteref=" + siteRef;
    url = url + "&keywordsession=" + keywordsession;
    xmlHttp.onreadystatechange = stateChanged;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
};

function doQgoNoMatchClick(handleId, link) {
    noMatchLink = link;
    xmlHttp = GetXmlHttpObject();

    if (xmlHttp == null) {
        // unable to register the click since this browser does not support http request.
        return;
    } 

    var url = "/web/wcbservlet/com.gxwebmanager.gxpublic.qgoelement.servlet";
    url = url + "?clicktype=nomatch";
    url = url + "&handleid=" + handleId;
    xmlHttp.onreadystatechange = noMatchStateChanged;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
};

function noMatchStateChanged() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        // click is submitted to serlvet and received a 200 response code.
        window.location = noMatchLink;
    }
};

function doLinkClick(keyword, pageTitle, linkurl, alias, siteRef, handleId) {
    xmlHttp = GetXmlHttpObject();

    if (xmlHttp == null) {
        // unable to register the click since this browser does not support http request.
        return;
    } 

    var url = "/web/wcbservlet/com.gxwebmanager.gxpublic.qgoelement.servlet";
    url = url + "?clicktype=link";
    url = url + "&keyword=" + keyword;
    url = url + "&pagetitle=" + pageTitle;
    url = url + "&handleid=" + handleId;
    if (typeof escape == 'function') {
        url = url + "&url=" + escape(linkurl);
    } else {
        url = url + "&url=" + linkurl;
    }
    url = url + "&alias=" + alias;
    if (siteRef != 'undefined') {
        url = url + "&siteref=" + siteRef;
    }
    xmlHttp.onreadystatechange = stateChanged;
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
};

function stateChanged() {
    if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
        // click is submitted to serlvet and received a 200 response code.
    }
};

function GetXmlHttpObject() {
    var xmlHttp = null;
    try {
        // Firefox, Opera 8.0+, Safari
        xmlHttp = new XMLHttpRequest();
    } catch (e) {
        // Internet Explorer
        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
};

function updateSearchResultsLinks() {
    var allElements = document.getElementsByClassName('qgoclick');
    for (var i = 0; i < allElements.length; i++) {
        var link = allElements[i].href;
        allElements[i].href = '#';
        allElements[i].onclick = allElements[i].onclick + '; document.href='+ link + ';';
    }
};

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            func();
            if (oldonload) {
                oldonload();
            }
        }
    }
};
addLoadEvent(updateSearchResultsLinks);