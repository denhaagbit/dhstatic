/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                               */
/*                                                                          */
/* Filename:                $File: jq.function.js $               */
/* Last changed by:         $Author: bslokxr $                              */
/* Last modified:           $Date: 2012-02-07 19:11:48 +0100 (di, 07 feb 2012) $                                          */
/* Revision number:         $Rev: 53 $                                      */
/*                                                                          */
/****************************************************************************/

$(document).ready(function(){
	// dropdown lang
	$('.itemLang ul').dropDownDh({activeClass:'activeItem'});
	$('.unitListHeader li.itemSize').cssDh();
	// top form text
	$('.unitFormHeader form').seacrhDh();
	// columns & dropdown menu
	$('.assemblyMenu .unitSubMenu').columnDh();
	$('.assemblyMenu .unitSubMenu').dropDownDh({activeClass:'itemHover'});
	// transparent bottom block news
	//$('.stationMediumImg ul').opacityBlockDh({opacityValue:0.9});
	// transparent logo in news
	$('.stationMediumImg .unitLogo').transparentBlockDh();
	
	//$('.searchFAQ').accordDh();
	if ($('.searchFAQ').length){
        $('.searchFAQ dd div').hide();
        $('.searchFAQ dt select').live('change', function(obj){
            $('.searchFAQ dd, .searchFAQ dd div').hide();
            $(obj.target).parents('dt').next().show();
            $('#'+$(obj.target).val()).show();
        });
        $('.searchFAQ dt a').live('click', function(obj){
            if ($($(obj.target).parents('dt').next()).attr('class') == 'visible'){
                $(obj.target).parents('dt').next().hide();
                $('#'+$(obj.target).next().val()).hide();
                $(obj.target).parents('dt').next().removeAttr('class');
            } else {
                $('.searchFAQ dd, .searchFAQ dd div').hide();
                $(obj.target).parents('dt').next().show();
                $('#'+$(obj.target).next().val()).show();
                $(obj.target).parents('dt').next().attr('class', 'visible');
            }
        });
    };
	
	$('.assemblySub').olDh();
	$('.artikelUsefulInfo .itemLink a').popupDh({contentBlock:'#tip_redactie', positionBlock:['top', -335]});
	$('.pageToolBox a[href="#stuur_artikel"], .fotoToolBox a[href="#stuur_artikel"]').popupDh({contentBlock:'#stuur_artikel', positionBlock:['top', -25]});
	$('.pageToolBox a[href="#toevoegen_aan"]').popupDh({contentBlock:'#toevoegen_aan', positionBlock:['bottom', -40]});
	$('.fotoToolBox a[href="#toevoegen_aan"]').popupDh({contentBlock:'#toevoegen_aan', positionBlock:['bottom', -40]});
	$('.artikelUsefulInfo').infoFormDh();
	$('.itemHeaderLogo').slideLogoDh();
	
	$('.sideCallCentre a').callCenterDh();
	
	$('a.openPopup').openPopupDh();
	$('.calendarDayCell .event div').tooltipDh();
	//make textarea empty onclick, focus
	$(".stationForm textarea, .hideForm textarea").one('click focus', function(){
		$(this).val('');
	});
	$('.toolBoxPrint').printLinkDh();
	
	$('.unitFormRis form, .searchForm form').seacrhDh();
    $('.risAgn').accordRisDh();
    $('.assemblyRis .stationForm a.itemHelp').tooltipDh({tipClass:'viewTooltipRis'});
});
if(jQuery) (function($){
	$.fn.slideLogoDh = function(params){
		var conf = $.extend({
			itemBlock:'li',
			moveBlock:'ul',
			startItem:0,
			playDuration:5000,
			fadeTime:500
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),f=this,i=c.startItem,t=null;
			$.extend(f,{
				getSize:function(){return o.find(c.itemBlock).size()},
				getMoveBlock:function(){return o.find(c.moveBlock)},
				getWidthItem:function(){return f.getMoveBlock().find(c.itemBlock).outerWidth(true)},
				moveItem:function(k){
					f.getMoveBlock().animate({opacity:0}, c.fadeTime,
						function(){
							if(i>=f.getSize()){
								f.getMoveBlock().css({left:0}); i=0;
							}else{
								f.getMoveBlock().css({left:-(i*f.getWidthItem()-1)})
							}
							f.getMoveBlock().animate({opacity:1}, c.fadeTime);
						}
					);
				},
				autoRestart:function(){
					clearTimeout(t);
					t=setTimeout(function(){
						f.moveItem(++i);
						f.autoRestart();
					},c.playDuration);
				},
				autoPause:function(){
					clearInterval(t);
				}
			});
			if((f.getSize()>1) && ((c.fadeTime*2)<c.playDuration)){
				f.getMoveBlock().addClass('listScriptLoad');
				f.autoRestart();
				o.hover(
					function(){f.autoPause()},function(){f.autoRestart()}
				)
			}
		})
	};
	$.fn.callCenterDh = function(params){
		var conf = $.extend({
			hideClass:'hideElement'
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),t=o.attr('href');
			if($(t)){
				$(t).addClass(c.hideClass);
				o.bind('click', function(){
					if($(t).hasClass(c.hideClass)){$(t).removeClass(c.hideClass); return false}
					else{$(t).addClass(c.hideClass); return false}
				})
			}
		})
	};
	$.fn.openPopupDh = function(params){
		var conf = $.extend({
			width:600,
			height:700
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),t=o.attr('href');
			o.bind('click', function(){
				window.open(t, '', 'width='+c.width+',height='+c.height+',scrollbars=1'); return false;
			})
			
		})
	};
	$.fn.printLinkDh = function(params){
		var conf = $.extend({
			parentClass:'hideElement'
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),p=o.parent();
			if(p.hasClass(c.parentClass)){
				p.removeClass(c.parentClass);
				o.bind('click', function(){window.print(); return false;});
			}
		})
	};
	$.fn.infoFormDh = function(params){
		var conf = $.extend({
			headerBlock:'input:radio',
			contentBlock:'fieldset'
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),a=o.find(c.headerBlock),b=o.find(c.contentBlock);
			if((b != null) && (a != null)){
				b.addClass('hideElement');
			}
			a.one('click', function(){
				b.removeClass('hideElement');
			})
		})
	};
	$.fn.olDh = function(params){
		var conf = $.extend({
			numberClass:'itemNumber'
		}, params);
		return this.each(function(){
			var c=conf,o=$('ol', this),f=this;
			$.extend(f,{
				addElement:function(e,c,p,t){
					var i=document.createElement(e);
					i=$(i).addClass(c);
					if(t){p.append(i)}else{p.prepend(i)}
					return i;
				}
			});
			o.each(function(){
				$(this).addClass('noListType');
				$(this).children().each(function(i){
					i++;
					if($(this).find('.'+c.numberClass).html() != null){
						$(this).find('.'+c.numberClass).append(i+'.');
					}else{
						f.addElement('span',c.numberClass,$(this)).html(i+'.');
						$('li', this).each(function(){
							f.addElement('span',c.numberClass,$(this)).html(i+'.');
						})
					};
				});
			})
		})
	};
	$.fn.cssDh = function(){
		return this.each(function(){
			var o=$(this),l=$(this).find('a'),a;
			o.addClass('showElement');
			l.each(function(i){
				if($(this).hasClass('itemActive')){a=i}
				$(this).click(function(e){
					$('link[title=changeFont]').attr('href', $(this).attr('rel'));
					$(this).addClass('itemActive');
					l.eq(a).removeClass('itemActive');
					a=i;
					e.preventDefault();
				})
			})
		})
	};
	$.fn.popupDh = function(params){
		var conf = $.extend({
			contentBlock:'div',
			linkClose:'.closePopup',
			activeClass:'itemActive',
			positionBlock:['top', 'left']
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),f=this,w=$(document).find(c.contentBlock),v=null,tp,lp,bm,bt,bb;
			$.extend(f,{
				bindClick:function(e){
					if(v==false){f.showItem(e)}else if(v==true){f.hideItem(e)}
				},
				loadItem:function(){
					w.appendTo('body');
					w.addClass('hidePopup');
					bt=$('<div />').addClass('btmAngle').prependTo(w).transparentBlockDh();
					bm=$('<div />').addClass('popupBg').prependTo(w).transparentBlockDh();
					bb=$('<div />').addClass('topAngle').prependTo(w).transparentBlockDh();
					f.bindClose();
					v=false;
				},
				showItem:function(){
					f.positionItem();
					v=true;
					o.addClass(c.activeClass);
				},
				hideItem:function(){
					v=false;
					o.removeClass(c.activeClass);
					w.removeAttr('style');
				},
				bindClose:function(){
					w.find(c.linkClose).bind('click', function(e){f.hideItem(e);e.preventDefault();})
				},
				positionItem:function(){
					bm.css({height:w.height()-bt.height()-bb.height()});
					if(c.positionBlock[0]=='top'){
						tp=o.offset().top-w.outerHeight();
					}else if(c.positionBlock[0]=='bottom'){
						tp=o.offset().top+o.outerHeight();
					}else{
						tp=o.offset().top+c.positionBlock[0];
					}
					if(c.positionBlock[1]=='left'){
						lp=o.offset().left-w.outerWidth();
					}else if(c.positionBlock[1]=='right'){
						lp=o.offset().left+o.outerWidth();
					}else{
						lp=o.offset().left+c.positionBlock[1];
					}
					w.css({top:Math.round(tp), left:Math.round(lp)});
				}
			});
			f.loadItem();
			o.bind('click', function(e){
				f.bindClick();
				e.preventDefault();
			});
		})
	};
	$.fn.accordDh = function(params){
		var conf = $.extend({
			headerBlock:'dt',
			contentBlock:'dd',
			openClass:'active',
			openItem:-1
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),f=this,h=o.children(c.headerBlock),d=-1;
			$.extend(f,{
				openItem:function(m){
					m.next().css({display:'block'});
					m.addClass(c.openClass);
				},
				closeItem:function(m){
					m.next().css({display:'none'});
					m.removeClass(c.openClass);
				}
			})
			h.each(function(i){
				var v=$(this);
				if(i==c.openItem){
					f.openItem(v);
					d=i;
				}
				if(v.next().is(c.contentBlock)){
					v.bind('click', function(e){
						if(d!=i){
							f.openItem(v);
							f.closeItem($(h[d]));
							d=i;
						}else{
							f.closeItem(v);
							d=-1;
						}
						e.preventDefault();
					})
				}
			})
			
		})
	};
	$.fn.transparentBlockDh = function(){
		return this.each(function(){
			var o=$(this),f=this,b,s='',c='',n=navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && (navigator.appVersion.indexOf("MSIE 5.5") != -1 || navigator.appVersion.indexOf("MSIE 6.0") != -1);
			if(n==true && $.browser.msie){
				if(f.tagName.toLowerCase()=='img' && o.attr('src').indexOf(".png")!=-1){
					o.attr('width',o.width());
					o.attr('height',o.height());
					c = (o.attr('id')) ? 'id="' + o.attr('id') + '" ' : '';
					c += (o.attr('class')) ? 'class="' + o.attr('class') + '" ' : '';
					c += (o.attr('title')) ? 'title="' + o.attr('title') + '" ' : '';
					c += (o.attr('alt')) ? 'alt="' + o.attr('alt') + '" ' : '';
					s += '<span '+ c + 'style="position:relative;display:inline-block;background:transparent;';
					s += 'width:' + o.width() + 'px;' + 'height:' + o.height() + 'px;';
					s += 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader' + '(src=\'' + o.attr('src') + '\', sizingMethod=\'scale\');"></span>';					
					o.after(s);
					o.remove();
				}else{
					b=o.css('backgroundImage');
					if(b.indexOf(".png")!=-1){
						b= b.split('url("')[1].split('")')[0];
						o.css('background-image', 'none');
						o.get(0).runtimeStyle.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + b + "',sizingMethod='scale')";
					}
				}
			}
		})
	};
	//
	$.fn.seacrhDh = function(params){
		var conf = $.extend({
			textInput:'input:text',
			valueDef:'span.valueText'
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),i=o.find(c.textInput),t=o.find(c.valueDef).html();
			if(t==null){t='Stel uw vraag'};
			if(i.val()==''){i.val(t)};
			o.submit(function(){
				if((i.val()==t)||(i.val()=='')){
					return false;
				}
			});
			i.blur(function(){
				if(i.val()==''){
					i.val(t);
				}
			});
			i.focus(function(){
				if(i.val()==t){
					i.val('');
				}
			});
		})
	};
	//
	$.fn.opacityBlockDh = function(params){
		var conf = $.extend({
			opacityValue:0.9
		}, params);
		return this.each(function(){
			var o=$(this),f=this,c=conf,b,g=o.css('backgroundColor');
			$.extend(f,{
				addElement:function(e,c,p,t){
					var i=document.createElement(e);
					i=$(i).addClass(c);
					if(t){p.append(i)}else{p.before(i)}
					return i;
				},
				addBackground:function(){
					b=f.addElement('div','',o,false);
					b.css({
						position:'absolute',
						zIndex:1,height:o.outerHeight(),
						width:o.outerWidth(),
						top:o.position().top,
						left:o.position().left,
						opacity:c.opacityValue,
						backgroundColor:g
					});
				}
			});
			o.css({backgroundColor:'transparent'});
			f.addBackground();
		})
	};
	$.fn.columnDh = function(params){
		var conf = $.extend({
			targetItem:'li',
			columnSize:2
		}, params);
		return this.each(function(){
			var o=$(this),c=conf,e={};
			e.l = o.children().size()-1;
			e.s = 0;
			$.each(o.children(), function(i) { if($(this).is(c.targetItem)) {e.s++} });
			e.m = Math.ceil(e.s/c.columnSize);
			e.t = e.m;
			e.g = o[0].tagName.toLowerCase();
			e.k = 0;
			e.n = $('<div class="'+o.attr('class')+'"></div>');
			e.i = $('<'+e.g+'></'+e.g+'>');
			$.each(o.children(), function(i) {
				if((e.k == e.t) && $(this).is(c.targetItem)) {
					e.t += e.m;
					$(e.n).append(e.i).insertBefore(o);
					e.i = $('<'+e.g+'></'+e.g+'>');
				}
				if($(this).is(c.targetItem)) e.k++;
				e.i.append($(this));
				if(i == e.l) { $(e.n).append(e.i).insertBefore(o)}
			});
			$(this).replaceWith(o.html());
		});
	};
	$.fn.dropDownDh = function(params){
		var conf = $.extend({
			bindAction:'hover',
			activeClass:'active'
		}, params);
		return this.each(function(){
			var o=$(this),c=conf,p=o.parent().eq(0),m=o.parent().children().eq(0);
			p.hoverIntent({
				timeout: 150,
				over: function(){
					if(c.bindAction == 'hover'){
						o.css('opacity', 0).css('visibility', 'visible').animate({opacity:1}, 100);
						p.addClass(c.activeClass);
					}
				},
				out: function(){
					o.animate({opacity:0}, 100, function(){
						o.css('visibility', 'hidden');
						p.children().eq(0).removeClass('active');
						p.removeClass(c.activeClass);
					});
					
				}
			});
			if(c.bindAction == 'click'){
				m.click(function(){
					if(o.css('visibility') == 'hidden') {
						o.css('opacity', 0).css('visibility', 'visible').animate({opacity:1}, 100);
						m.addClass(c.activeClass);
					} else {
						o.animate({opacity:0}, 100, function(){
							o.css('visibility', 'hidden');
							m.removeClass(c.activeClass);
						});
					}
					return false;
				});
			}
		});
	};
	//
    $.fn.accordRisDh = function(params){
        var conf = $.extend({
            headerBlock:'dt',
            contentBlock:'dd',
            openClass:'active',
            nameId:'accord-',
            textOpen:'Alles uitklappen',
            textClose:'Alles inklappen'
        }, params);
        return this.each(function(){
            var c=conf,o=$(this),f=this,a=0,s=0,sa=new Array(),l=$('<a href="#"></a>');
            $.extend(f,{
                openItem:function(hI,bI,e){
                    if(e=='open'){hI.addClass('itemOpen').removeClass('itemClose');bI.slideDown();s+=1;
                    }else{hI.addClass('itemClose').removeClass('itemOpen');bI.slideUp();s-=1;
                    }
                    if(sa.length==s){f.linkChange(l,c.textClose)}else{f.linkChange(l,c.textOpen)}
                },
                openAll:function(){
                    if(sa.length==s){
                        s=sa.length;
                        $.each(sa,function(){f.openItem($(this).children('dt'),$(this).children('dd'),'close');});
                    }else{
                        s=0;
                        $.each(sa,function(){f.openItem($(this).children('dt'),$(this).children('dd'),'open');});
                    }
                },
                findBlock:function(){return o.children('dl')},
                linkChange:function(lnk, txt){lnk.text(txt).attr('title', txt)},
                openChange:function(){
                    f.findBlock().each(function(){
                        if($(this).children('dd').html()!=''){sa.push($(this))}
                    });
                    f.linkChange(l,c.textOpen);
                    $('p.itemSort', o).append(l);
                    l.bind('click', function(){f.openAll();return false;})
                }
            });
            f.findBlock().each(function(i){
                var elBlock=$(this), elHead=elBlock.children('dt'), elBody=elBlock.children('dd');
                elBody.css({display:'none'});
                if(elBody.html()!=''){
                    var elLink=$('<a href="#'+c.nameId+i+'"></a>').html(elHead.html());
                    elHead.empty().append(elLink); elBody.attr('id', c.nameId+i);
                    elHead.addClass('itemClose');
                    elHead.bind('click', function(e){
                        if(elHead.hasClass('itemClose')){
                            f.openItem(elHead,elBody,'open');
                        }else{
                            f.openItem(elHead,elBody,'close');
                        }
                        e.preventDefault()
                    }).bind('mouseenter focusin', function(){elHead.addClass('itemHover')
                    }).bind('mouseleave focusout', function(){elHead.removeClass('itemHover')})
                }
            });
            f.openChange();
        })
    };
})(jQuery);