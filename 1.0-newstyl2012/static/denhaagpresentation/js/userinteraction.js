/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                                          */
/*                                                                          */
/* Filename:                $File: userinteraction.js $               */
/* Last changed by:         $Author: bslokxr $                              */
/* Last modified:           $Date: 2012-01-11 09:45:39 +0100 (Wed, 11 Jan 2012) $ */
/* Revision number:         $Rev: 37 $                                      */
/*                                                                          */
/****************************************************************************/

function submitRating(contentId,id){
	document.getElementById('vote'+contentId).value=id;
	document.getElementById('votingform'+contentId).submit();
}