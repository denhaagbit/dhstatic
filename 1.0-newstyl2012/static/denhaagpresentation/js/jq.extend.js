/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                               */
/*                                                                          */
/* Filename:                $File: jq.extend.js $               */
/* Last changed by:         $Author: bslokxr $                              */
/* Last modified:           $Date: 2013-04-02 19:54:39 +0200 (Tue, 02 Apr 2013) $                                          */
/* Revision number:         $Rev: 249 $                                      */
/*                                                                          */
/****************************************************************************/

/*
 * New unobtrusive application function library based on plugin structures
 */
var denhaag = {};

(function($) {
    
    denhaag.plugins = 
    {
        init : function ()
        {
            // Helper functions and classes
            if (document.getElementsByTagName)
            {
                document.getElementsByTagName('html')[0].className = "js-on";
            };
            // Bind tooltip
            if($('.dh-bh-tooltip').length)
            {
                denhaag.plugins.Tooltip.init();
            };
            // Bind rich tooltip
            if($('.dh-bh-richtooltip').length > 0)
            {
                denhaag.plugins.richTooltip.init();
            };
            // Bind dialog
            if($('.dh-bh-dialog').length)
            {
                denhaag.plugins.dialog.init();
            };
            //Bind datePicker
            if($('input.datepicker').length)
            {
                denhaag.plugins.datePicker.init();
            };
            //Dynamic elements in Toolbox
            if($('.pageToolBox').length)
            {
                denhaag.plugins.dynamicToolbox.init();
            };
            //Bind Accordion
            if($('.bh-accordion').length)
            {
                denhaag.plugins.accordion.init();
            };
        }
    };
    
    /*
     * Create rich tooltip
     */
    denhaag.plugins.Tooltip =
    {
        init : function ()
        {
                    $('.dh-bh-tooltip').tooltip({
                        track: true,
                        delay: 0,
                        showURL: false,
                        //fixPNG: true,
                        //showBody: " - ",
                        extraClass: 'rich-tooltip',
                        
                        top: -20, /*15*/
                        left: 15 /*-193*/
                    });
        }
    };
    /*
     * Create rich tooltip
     */
    denhaag.plugins.richTooltip =
    {
        init : function ()
        {
            /*$('map.dh-bh-richtooltip > area').each(function(index, obj) {
                if (!$.data(obj,'tooltip')) {*/   // only if no tooltip is attached
                    $('map.dh-bh-richtooltip > area').tooltip({
                        track: true,
                        delay: 0,
                        showURL: false,
                        //fixPNG: true,
                        //showBody: " - ",
                        extraClass: 'rich-tooltip',
                        /*bodyHandler: function() {
                            if (this.tooltipText) {
                                return $("<h3/>").html(this.tooltipText);
                            } else {
                                return $(this).html();
                            }
                        }, */
                        top: -20, /*15*/
                        left: 15 /*-193*/
                    });
                /*};
            });*/
        }
    };
    /*
     * Den Haag: create rich dialog function
     */
    denhaag.plugins.dialog =
    {
        init : function ()
        {
            $('a.dh-bh-dialog').live('click', function () {
                if ($(this).attr('href').length) {                    
                    denhaag.plugins.dialog.write(this);
                    return false;
                }
            });
            
        },
        write : function (obj)
        {
            $($(obj).attr('href')).dialog({
                autoOpen: false,
                width: 768,
                height: 500,
                closeText: 'Sluit',
                draggable: false,
                resizable: false,
                modal: true,
                open: function () {
                    $(".ui-widget-overlay").click(function()
                    {
                        $($(obj).attr('href')).dialog('close');
                    });
                }
            });
            
            $($(obj).attr('href')).dialog('open');
        }
    };
    /*
     * Create datepicker
     */
    denhaag.plugins.datePicker =
    {
        init : function ()
        {
            $('input.datepicker, .datepicker input').each(function() {
                $(this).datepicker({
                    dateFormat: "dd-mm-yy",
                    changeMonth: true,
                    changeYear: true,

                    dayNamesMin: ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
                    monthNamesShort: ['jan','feb','mrt','apr','mei','jun','jul','aug','sep','okt','nov','dec'],
                    yearRange: '1911:2051',

                    showOn: "both",
                    buttonImage: "/static/denhaagpresentation/images/css/icon_datepicker.png",
                    buttonText: "kalender",
                    buttonImageOnly: true,
                    beforeShow: function(i,e){
                        $.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset}});
                        setTimeout(function(){
                            e.dpDiv.css('z-index',100);
                        },10);
                    },
                    onSelect: function(dateStr) {
                        $('.ui-datepicker a').removeAttr('href');
                    },
                    firstDay: 1,
                    minDate: 
                        $(this).closest('.datepicker').hasClass('future_date') ? 1 : null ||
                        $(this).closest('.datepicker').hasClass('from-date') ? $(this).parent().find('.from-date').html() : null,
                    maxDate: 
                        $(this).closest('.datepicker').hasClass('min_age') ? 1 : null
                });
            });
        }
    };
    /*
     * Add dynamic content to pageToolbox
     */
    denhaag.plugins.dynamicToolbox =
    {
        init : function ()
        {
            $('.pageToolBox').each(function() {
                $('<li class="toolBoxSeparator"><a class="toolBoxPrint" title="print" href="javascript:window.print();"><img src="http://www.denhaag.nl/static/denhaagpresentation/images/printIcon.gif" alt="print">print</a></li>').insertAfter($(this).find('li:first'));
            });
        }
    };
    /*
     * Accordion
     */
    denhaag.plugins.accordion = 
    {
        init : function ()
        {
            $(".bh-accordion").accordion({
                active: false,
                collapsible: true
            });
        }
    };
    // Initialize denhaag plugins
    $(document).ready(function(){
        denhaag.plugins.init();
    });
    
})(jQuery);

$(document).ready(function(){
    /*$('.assemblyHeader .stationHeader:first').addClass('service-nav');
    $('.assemblyHeader .stationHeader:last-child').addClass('meta-nav');*/
    $('.listSmall li:last-child').addClass('last');
    $('.stationFour').each(function(index){$(this).addClass(index+'-row');});
    /*$('.assemblyMenu li:last-child.itemActive').addClass('itemActiveLast');*/
    if($('.itemHeaderLogo img').attr('alt')=='Gepubliceerd namens gemeente Den Haag'){$('.assemblyContainer ').addClass('publisher-denhaag');}
    else if($('.itemHeaderLogo img').attr('alt')=='product_detail_logo_title'){$('.assemblyContainer ').addClass('publisher-denhaag');}
    else if($('.itemHeaderLogo img').attr('alt')=='Logo gemeente Den Haag'){$('.assemblyContainer ').addClass('publisher-denhaag');};
    if($('.assemblyMenu .itemActive span').html() == 'home') {
        $('.assemblyMenu').css('border-bottom-color', 'transparent');
    }
    /*$('<span class="itemLeftside"></span>').prependTo($('.assemblyMenu .itemActive'));*/
   //var today = new Date('04/25/2012');
   //if (new Date().toDateString()==today.toDateString()){$('body').addClass('orange');};
   
   var today = new Date('06/08/2011 17:30:00');
   var todayEnd = new Date('06/17/2011 00:00:00')
   if (new Date() >= today && new Date() <= todayEnd) {
       $('body').addClass('orange');
   }
   
   //console.log(today, new Date());
   
});


$(window).load(function(){
    $('.0-row .unitStation').syncHeight();
    $('.1-row .unitStation').syncHeight();
    $('.2-row .unitStation').syncHeight();
    $('.3-row .unitStation').syncHeight();
    $('.4-row .unitStation').syncHeight();
    
    /*var totalmargin = $(window).width() - 960;
    var margin = ($(window).width() - 960) / 2;
    
    $('.assemblyHeader').attr('style', 'margin-left: -'+ margin +'px;margin-right: -'+ margin +'px;padding-left: '+ margin +'px;padding-right: '+ margin +'px;');
    */
});

if(jQuery) (function($){
    var getHeightProperty = function() {
        var browser_id = 0;
        var property = [
            ['min-height','0px'],
            ['height','1%']
        ];

        if($.browser.msie && $.browser.version < 7){
            browser_id = 1;
        }
        
        return { 'name': property[browser_id][0], 
                 'autoheightVal': property[browser_id][1] };
    };
    
    $.getSyncedHeight = function(selector) {
        var max = 0;
        var heightProperty = getHeightProperty();
        $(selector).each(function() {
            $(this).css(heightProperty.name, heightProperty.autoheightVal);
            var val = $(this).height();
            if(val > max){
               max = val;
            }
        });
        return max;
    };
    
    $.fn.syncHeight = function(config) {
        var defaults = {
            updateOnResize: false,
            height: false
        };
        var options = $.extend(defaults, config);

        var e = this;

        var max = 0;
        var heightPropertyName = getHeightProperty().name;

        if(typeof(options.height) === "number") {
            max = options.height;
        } else {
            max = $.getSyncedHeight(this);
        }
        $(this).each(function() {
            $(this).css(heightPropertyName, max+'px');
        });

        if (options.updateOnResize === true) {
            $(window).resize(function(){ 
                $(e).syncHeight();
            });
        }
        return this;
    }; 
    
	$.event.special.imgload={
		add:function(hollaback){
			if(this.nodeType===1&&this.tagName.toLowerCase()==='img'&&this.src!==''){
				if(this.complete||this.readyState===4){
					hollaback.handler.apply(this);
				}else if(this.readyState==='uninitialized'&&this.src.indexOf('data:')===0){
					$(this).trigger('error');
				}else{
					$(this).bind('load', hollaback.handler);
				}
			}
		}
	};
	$.fn.carouselFotoDh = function(params){
		var conf = $.extend({
			clipBlock:'.clipCarousel',
			moveBlock:'ul',
			itemBlock:'li',
			prevPage:'itemArrowLeft',
			nextPage:'itemArrowRight',
			viewItem:'unitSingle',
			currentBlock:'unitCurrent',
			playBlock:'unitSlide',
			playDuration:6000,
			timeAnimate:300,
			nWidth:707,
			nHeight:472,
			labelPlay:'',
			labelStop:'',
			labelPrev:'',
			labelNext:'',
			labelFotoImg:'',
			labelFromImg:''
		}, params);
		return this.each(function(){
			var c=conf, o=$(this), f=this, t1=null, view=$('<div />').addClass(c.viewItem), fake=$('<div />').addClass('unitFake'), prev=$('<a />').addClass(c.prevPage).attr('title', c.labelPrev).html(c.labelPrev),
				next=$('<a />').addClass(c.nextPage).attr('title', c.labelNext).html(c.labelNext), currentPosition, currentItem=-1, preload=[], cur=$('<div />').addClass(c.currentBlock), slide=$('<a href="#" title="'+c.labelPlay+'">'+c.labelPlay+'</a>').addClass(c.playBlock), slideInfo=false, fakeList=$('<div />');
			$.extend(f,{
				getClip:function(){return o.find(c.clipBlock)},
				getMoveBlock:function(){return f.getClip().find(c.moveBlock)},
				getLastItem:function(){
					var p=f.getAllItem().eq(f.getSizeItem()-1);
					return f.getClip().outerWidth(false)-(p.outerWidth(false)+p.position().left)},
				getCenterItem:function(itemIn){
					var p=f.getAllItem().eq(itemIn);
					return p.outerWidth(false)/2+p.position().left;
				},
				getSizeItem:function(){return f.getAllItem().size()},
				getAllItem:function(){return f.getMoveBlock().children(c.itemBlock)},
				getFocus:function(i){return fakeList.children('a').eq(i).focus()},
				pushAll:function(){
					f.getAllItem().each(function(i){
						var desc=$('<div />').addClass('unitDesc');
						preload.push(desc.append($(this).find('.unitText')));
					});
				},
				moveItem:function(itemIn){
					currentPosition=-f.getCenterItem(itemIn)+f.getClip().outerWidth(false)/2;
					prev.removeClass('disableArrow'); next.removeClass('disableArrow');
					if(currentPosition<f.getLastItem()){currentPosition=f.getLastItem(); next.addClass('disableArrow')}
					if(currentPosition>0){currentPosition=0; prev.addClass('disableArrow')}
					if(currentItem!=itemIn){
						f.getAllItem().removeClass('itemActive');
						currentItem=itemIn;
						f.getMoveBlock().animate({left:currentPosition});
						f.loadingItem(itemIn);
						f.viewCurrent(currentItem);
					}
					f.getAllItem().eq(currentItem).addClass('itemActive');
				},
				resizeImg:function(imgSub, needHeight, needWidth){
					var aspectRatio=needWidth/needHeight, thisWidth=imgSub.width(), thisHeight=imgSub.height(), imgAspectRatio=thisWidth/thisHeight, topSize=0, okHeight=needHeight;
					//if(needHeight===0){aspectRatio=imgAspectRatio-1}else{aspectRatio=imgAspectRatio+1}
					//if((needHeight&&thisHeight>needHeight)||(needWidth&&thisWidth>needWidth)){
						if(imgAspectRatio>aspectRatio){needHeight=Math.floor(thisHeight/thisWidth*needWidth);
						}else{needWidth=Math.floor(thisWidth/thisHeight*needHeight)}
						if(needHeight<okHeight){topSize=(okHeight-needHeight)/2}
						$(imgSub).css({height:needHeight, width:needWidth, marginTop:topSize});
					//}
				},
				loadingItem:function(itemLink){
					fake.removeClass('hideElement').animate({opacity:1}, c.timeAnimate, function(){
						$('<img />').attr('src', f.getAllItem().eq(itemLink).find('a').attr('href')).bind('imgload',
							function(e){
								view.empty().append(preload[itemLink]);
								f.resizeImg($(this).appendTo(view), c.nHeight-preload[itemLink].height(), c.nWidth);
								$(this).appendTo(view);
								fake.animate({opacity:0}, c.timeAnimate, function(){fake.addClass('hideElement')});
							});
					});
				},
				movePage:function(toSide){
					if(toSide=='minus'){
						currentPosition+=f.getClip().outerWidth(false);
						if(currentPosition>0){currentPosition=0; prev.addClass('disableArrow')}
					}else if(toSide=='plus'){
						currentPosition-=f.getClip().outerWidth(false);
						if(currentPosition<f.getLastItem()){currentPosition=f.getLastItem(); next.addClass('disableArrow')}
					}
					f.getMoveBlock().animate({left:currentPosition});
				},
				slideShow:function(){
					if(slideInfo==false){
						slide.addClass('startSlide').html(c.labelStop).attr('title', c.labelStop);
						slideInfo=true; f.autoRestart();
						f.getClip().bind('mouseenter', function(){f.autoPause()}).bind('mouseleave',function(){f.autoRestart()});
						view.bind('mouseenter', function(){f.autoPause()}).bind('mouseleave',function(){f.autoRestart()});
					}else{
						slide.removeClass('startSlide').html(c.labelPlay).attr('title', c.labelPlay);
						slideInfo=false; f.autoPause();
						f.getClip().unbind('mouseenter mouseleave');
						view.unbind('mouseenter mouseleave');
					}
				},
				autoRestart:function(){clearTimeout(t1);
					if(currentItem+1>=f.getSizeItem()){currentItem=-1}
					t1=setTimeout(function(){
					f.getFocus(currentItem+1); f.moveItem(currentItem+1); f.autoRestart()},c.playDuration);
					
				},
				autoPause:function(){clearInterval(t1)},
				viewCurrent:function(i){cur.html(c.labelFotoImg+' '+(i+1)+' '+c.labelFromImg+' '+f.getSizeItem())}
			});
			f.getClip().addClass('clipCarouselLoad');
			f.getMoveBlock().addClass('listCarouselLoad');
			f.pushAll();
			o.prepend(fake).prepend(view).prepend(prev).append(next).find('.fotoToolBox').prepend(slide).prepend(cur);
			prev.bind('click', function(){
				next.removeClass('disableArrow');
				f.movePage('minus');
			});
			slide.bind('click', function(e){
				f.slideShow();
				e.preventDefault();
			});
			next.bind('click', function(){
				prev.removeClass('disableArrow');
				f.movePage('plus');
			});
			f.getAllItem().each(function(i){
				fakeList.append($('<a href="#">fake'+i+'</a>')).appendTo(f.getClip()).addClass('fakeLink');
				$(this).bind('click', function(e){
					f.moveItem(i); f.getFocus(i); e.preventDefault();
				}).find('a').attr('tabIndex', '-1');
			});
			fakeList.children('a').each(function(i){
				$(this).bind('focusin', function(){
					f.moveItem(i);
				});
			});
			f.moveItem(0);
			f.getClip().bind('keydown', function(e){
				if(e.keyCode==37){f.getFocus(currentItem-1); f.moveItem(currentItem-1); return false}
				if(e.keyCode==39){f.getFocus(currentItem+1); f.moveItem(currentItem+1); return false}
			});
		})
	};
	//
    $.fn.tooltipDh = function(params){
        var conf = $.extend({
            effect:'toggle',
            fadeOutSpeed:'normal',
            predelay:0,
            delay:30,
            opacity:1,
            tip:0,
            position:['center', 'right'],
            offset:[0, 0],
            relative:!1,
            cancelDefault:!0,
            events:{
                def:'click,click',
                input:'focus,blur',
                widget:'focus mouseenter,blur mouseleave',
                tooltip:''
            },
            layout:'<div/>',
            tipClass:'view-tip',
            externalHtml:'<div class="itemArrow"><span /></div>',
            innerClose:'<span class="itemClose" />'
        }, params);
        return this.each(function(){
            var b = {
                toggle:[function(a){
                    var b = this.getConf(),
                        c = this.getTip(),
                        d = b.opacity;
                    d < 1 && c.css({
                        opacity: d
                    }), c.show(), a.call()
                }, function (a) {
                    this.getTip().hide(), a.call()
                }],
                fade:[function(a){
                    var b = this.getConf();
                    this.getTip().fadeTo(b.fadeInSpeed, b.opacity, a)
                }, function (a) {
                    this.getTip().fadeOut(this.getConf().fadeOutSpeed, a)
                }]
            };
            var f = this, d = $(this), e = conf,
                g = d.add(f), v=0, c=$(e.innerClose),
                h, i = 0,
                j = 0,
                k = d.attr('title'),
                l = d.attr('href'),
                m = b[e.effect],
                n, o = d.is(':input'),
                p = o && d.is(':checkbox, :radio, select, :button, :submit'),
                q = d.attr('type'),
                r = e.events[q] || e.events[o ? p ? 'widget' : 'input' : 'def'];
            if (!m) throw 'Nonexistent effect \'' + e.effect + '\'';
            r = r.split(/,\s*/);
            if (r.length != 2) throw 'Tooltip: bad events configuration for ' + q;
            if(r[0]=='click' && r[1]=='click'){
                d.bind('click', function(a){
                    if(v){f.hide(a);v=0}else{f.show(a);v=1}
                    return false;
                })
            }else{
                d.bind(r[0], function(a){
                    clearTimeout(i), e.predelay ? j = setTimeout(function(){
                        f.show(a)
                    }, e.predelay) : f.show(a)
                }).bind(r[1], function (a) {
                    clearTimeout(j), e.delay ? i = setTimeout(function(){
                        f.hide(a)
                    }, e.delay) : f.hide(a)
                })
            };
            k && e.cancelDefault && (d.removeAttr('title'), d.data('title', k));
            if(l && l!='#'){(l=$(l), $(l).remove())}
            $.extend(f, {
                setPosition:function(b, c, d){
                    var e = d.relative ? b.position().top : b.offset().top,
                        f = d.relative ? b.position().left : b.offset().left,
                        g = d.position[0];
                    e -= c.outerHeight() - d.offset[0], f += b.outerWidth() + d.offset[1], /iPad/i.test(navigator.userAgent) && (e -= a(window).scrollTop());
                    var h = c.outerHeight() + b.outerHeight();
                    g == 'center' && (e += h / 2), g == 'bottom' && (e += h), g = d.position[1];
                    var i = c.outerWidth() + b.outerWidth();
                    g == 'center' && (f -= i / 2), g == 'left' && (f -= i);
                    return {
                        top: e,
                        left: f
                    }
                },
                close:function(z){
                    c.bind('click', function(a){f.hide(a);v=0;});
                },
                show:function(b){
                    if(!h){
                        l ? h = $(e.layout).append(e.externalHtml).append(c).addClass(e.tipClass).appendTo(document.body).hide().append(l) : e.tip ? h = $(e.tip).eq(0) : k ? h = $(e.layout).append(e.externalHtml).addClass(e.tipClass).appendTo(document.body).hide().append(k) : (h = d.next(), h.length || (h = d.parent().next()));
                        if(!h.length) throw 'Cannot find tooltip for ' + d
                    }
                    if(f.isShown()) return f;
                    
                    h.stop(!0, !0);
                    var o = f.setPosition(d, h, e);
                    e.tip && h.html(d.data('title')), b = b || a.Event(), b.type = 'onBeforeShow', g.trigger(b, [o]);
                    if (b.isDefaultPrevented()) return f;
                    o = f.setPosition(d, h, e), h.css({
                        position: 'absolute',
                        top: o.top,
                        left: o.left
                    }), n = !0, m[0].call(f, function () {
                        b.type = 'onShow', n = 'full', g.trigger(b)
                    });
                    var p = e.events.tooltip.split(/,\s*/);
                    h.data('__set') || (h.bind(p[0], function(){
                        clearTimeout(i), clearTimeout(j)
                    }), p[1] && !d.is('input:not(:checkbox, :radio), textarea') && h.bind(p[1], function(a){
                        a.relatedTarget != d[0] && d.trigger(r[1].split(' ')[0])
                    }), h.data('__set', !0));
                    if(c.length){f.close(b)}
                    return f
                },
                hide:function(c){
                    if(!h || !f.isShown()) return f;
                    c = c || a.Event(), c.type = 'onBeforeHide', g.trigger(c);
                    if(!c.isDefaultPrevented()){
                        n = !1, b[e.effect][1].call(f, function(){
                            c.type = 'onHide', g.trigger(c)
                        });
                        return f
                    }
                },
                isShown:function(a){return a ? n == 'full' : n},
                getConf:function(){return e},
                getTip:function(){return h},
                getTrigger:function(){return d}
            }),
            $.each('onHide,onBeforeShow,onShow,onBeforeHide'.split(','), function(b, c){
                $.isFunction(e[c]) && $(f).bind(c, e[c]), f[c] = function(b){
                    b && $(f).bind(c, b);
                    return f
                }
            })//,
            //d.bind('click', function(){return false})
        })
    }
    //
    $.fn.tooltipDh2=function(params){
        var conf = $.extend({
            fadeOutSpeed:'fast',
            predelay:0,
            delay:30,
            opacity:1,
            data:'.tooltipIn',
            effect:'toggle',
            offset:[0, 11],
            position:['center','right'],
            events:['mouseenter', 'mouseleave'],
            layoutTip:'<div/>',
            layoutAppend:'<div class="itemArrow"><span /></div>',
            classTip:'viewTooltip',
            classHide:'hideElement'
        }, params);
        return this.each(function(){
            var effects={ 
                toggle:[ 
                    function(done){var conf=this.getConf(), tip=this.getTip(), o=c.opacity; if(o<1){tip.css({opacity:o})} tip.show(); done.call()},
                    function(done){this.getTip().hide(); done.call()} 
                ],
                fade:[
                    function(done){var conf=this.getConf(); this.getTip().fadeTo(c.fadeInSpeed, c.opacity, done)},
                    function(done){this.getTip().fadeOut(this.getConf().fadeOutSpeed, done)} 
                ]
            };
            var o=$(this), f=this, c=conf, timer=0, pretimer=0, tip, shown, data=o.find(c.data), fire=o.add(f), effect=effects[c.effect];
            if(typeof c.position=='string'){c.position=c.position.split(/,?\s/)}
            if(data){data.remove()}
            $.extend(f,{
                getPosition:function(){
                    var top=o.offset().top, left=o.offset().left, pos=c.position[0];
                    top-=tip.outerHeight()-c.offset[0];
                    left+=o.outerWidth()+c.offset[1];
                    var height=tip.outerHeight()+o.outerHeight();
                    if(pos=='center'){top+=height/2}
                    if(pos=='bottom'){top+=height}
                    pos=c.position[1];  
                    var width=tip.outerWidth()+o.outerWidth();
                    
                    if(pos=='center'){left-=width/2}
                    if(pos=='left'){left-=width}
                    return {top:top, left:left};
                },
                hide:function(e){
                    if(!tip||!f.isShown()){return f}
                    e=e||$.Event();
                    e.type='onBeforeHide';
                    fire.trigger(e);
                    if(e.isDefaultPrevented()){return}
                    shown=false;
                    effects[c.effect][1].call(f, function(){
                        e.type='onHide'; shown=false; fire.trigger(e);
                    });
                    return f;
                },
                show:function(e){
                    if(!tip){
                        if(data && data.length){
                            tip = $(c.layoutTip).addClass(c.classTip).appendTo('body').hide().append(data);
                            tip.append(c.layoutAppend);
                        }
                        if(!tip.length){throw 'Cannot find tooltip for ' + o}
                    }
                    if(f.isShown()){return f}
                    tip.stop(true, true);
                    var pos=f.getPosition();
                    e=e||$.Event();
                    e.type='onBeforeShow';
                    fire.trigger(e, [pos]);
                    if(e.isDefaultPrevented()){return f}
                    pos=f.getPosition();
                    tip.css({position:'absolute', top: pos.top, left: pos.left});
                    shown=true;
                    effect[0].call(f, function(){e.type='onShow'; shown='full'; fire.trigger(e)});
                    tip.bind(c.events[0], function(){
                        clearTimeout(timer); clearTimeout(pretimer);
                    });
                    tip.bind(c.events[1], function(e){
                        if(e.relatedTarget!=o[0]){o.trigger(c.events[1].split(' ')[0])}
                    });
                    return f;
                },
                isShown:function(fully){return fully ? shown == 'full' : shown},
                getConf:function(){return c},
                getTip:function(){return tip},
                getTrigger:function(){return o}
            });
            o.bind(c.events[0], function(e){
                clearTimeout(timer);
                if(c.predelay){pretimer=setTimeout(function(){f.show(e)}, c.predelay)}else{f.show(e)}
            }).bind(c.events[1], function(e){
                clearTimeout(pretimer);
                if(c.delay){timer=setTimeout(function(){f.hide(e)}, c.delay)}else{f.hide(e)}
            });
            $.each('onHide,onBeforeShow,onShow,onBeforeHide'.split(','), function(i, name){
                if($.isFunction(c[name])){$(f).bind(name, c[name])}
                f[name]=function(fn){
                    $(f).bind(name, fn); return f;
                };
            });
        });
    };
	//
	$.fn.carouselSmallDh = function(params){
		var conf = $.extend({
			clipBlock:'.carouselCnt',
			moveBlock:'ul.listSmall',
			itemBlock:'li',
			listClass:'carouselNbr',
			playDuration:5000,
			playBlock:'unitSlide',
			labelPlay:'',
			labelStop:''
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),
				f=this,
				tO=null,
				blockNav=$('<div />').addClass(c.listClass),
				fake=$('<div />').addClass('unitFake'), slideInfo=false,
				//fakeTab=$('<a href="">fake</a>').addClass('linkFake'),
				listNav, slide=$('<a href="#" title="'+c.labelPlay+'">'+c.labelPlay+'</a>').addClass(c.playBlock),
				currentItem=0;
			$.extend(f,{
				getClip:function(){return o.find(c.clipBlock)},
				getMoveBlock:function(){return f.getClip().find(c.moveBlock)},
				getWidthItem:function(){return f.getMoveBlock().find(c.itemBlock).outerWidth(true)},
				getAllItem:function(){return f.getMoveBlock().children(c.itemBlock)},
				getSizeItem:function(){return f.getAllItem().size()},
				moveItem:function(){
					if(currentItem>=f.getSizeItem()){currentItem=0}
					if(currentItem<0){currentItem=f.getSizeItem()-1}
					fake.animate({opacity:1}, 100, function(){
						f.getMoveBlock().css({left:-currentItem*f.getWidthItem()});
						fake.removeClass('hideElement');
						listNav.removeClass('itemActive').eq(currentItem).addClass('itemActive');
						fake.animate({opacity:0}, 300, function(){fake.addClass('hideElement')});
					});
				},
				listBlock:function(){
					f.getAllItem().each(function(i){
						blockNav.append($('<li />').html('<a href="#">'+(i+1)+'</a>'));
					}).find('a').attr('tabIndex', '-1');;
					blockNav.wrapInner('<ul />').prepend(slide);
					listNav=blockNav.find('li');
					listNav.each(function(index){
						$(this).bind('focusin', function(e){currentItem=index; f.moveItem();
						}).bind('click', function(e){e.preventDefault()})
					})
					o.append(blockNav);
				},
				slideShow:function(){
					if(slideInfo==false){
						slide.addClass('startSlide').html(c.labelStop).attr('title', c.labelStop);
						slideInfo=true; f.autoRestart();
						o.bind('mouseenter focusin', function(){f.autoPause()}).bind('mouseleave focusout',function(){f.autoRestart()});
					}else{
						slide.removeClass('startSlide').html(c.labelPlay).attr('title', c.labelPlay);
						slideInfo=false; f.autoPause();
						o.unbind('mouseenter mouseleave focusin focusout');
					}
				},
				autoRestart:function(){clearTimeout(tO); tO=setTimeout(function(){++currentItem; f.moveItem(); f.autoRestart()},c.playDuration)},
				autoPause:function(){clearInterval(tO)}
			});
			if(f.getClip().html()!=null){
				f.getClip().addClass('clipScriptLoad');
				f.getClip().prepend(fake);
				f.listBlock(); f.moveItem();
				listNav.bind('keydown', function(e){
					if(e.keyCode==37){f.moveItem(--currentItem); listNav.children('a').eq(currentItem).focus(); return false}
					if(e.keyCode==39){f.moveItem(++currentItem); listNav.children('a').eq(currentItem).focus(); return false}
					if(e.keyCode==13){
						if(f.getAllItem().eq(currentItem).find('a')!=null){
							document.location=f.getAllItem().eq(currentItem).find('a').attr('href'); return false
						}
					}
				});
				slide.bind('click', function(e){f.slideShow(); e.preventDefault()});
				f.slideShow();
			}
		})
	};
	//
	$.fn.carouselDh = function(params){
		var conf = $.extend({
			clipBlock:'.clipCarousel',
			moveBlock:'ul',
			itemBlock:'li',
			sizePage:5,
			prevPage:'itemArrowLeft',
			nextPage:'itemArrowRight',
			pageBlock:'.page',
			noScrollSize:120,
			sizeScroll:3,
			labelPrev:'',
			labelNext:''
		}, params);
		return this.each(function(){
			var c=conf,o=$(this),
				f=this,
				prev=$('<a />').addClass(c.prevPage).attr('title', c.labelPrev).html(c.labelPrev),
				next=$('<a />').addClass(c.nextPage).attr('title', c.labelNext).html(c.labelNext),
				currentItem=0,
				currentPage=0,
				positionMoveBlock=0,
				timeOut=null, pagePosition=null;
			$.extend(f,{
				getClip:function(){return o.find(c.clipBlock)},
				getMoveBlock:function(){return o.find(c.moveBlock)},
				getWidthItem:function(){return f.getMoveBlock().find(c.itemBlock).outerWidth(true)},
				getSizeItem:function(){return f.getAllItem().size()},
				getAllItem:function(){return f.getMoveBlock().children(c.itemBlock)},
				getSizePrevScroll:function(){return f.getClip().width()/2-c.noScrollSize+f.getClip().offset().left},
				getSizeNextScroll:function(){return f.getClip().width()/2+c.noScrollSize+f.getClip().offset().left},
				getLastPage:function(){return (Math.ceil((f.getSizeItem()-c.sizePage)))*f.getWidthItem()},
				movePage:function(pageIn){
					currentItem=pageIn*c.sizePage;
					if(currentItem<=0){currentItem=0; currentPage=0; positionMoveBlock=0; prev.addClass('disableArrow')}
					if(currentItem>=f.getSizeItem()-c.sizePage){
						currentItem=f.getSizeItem()-c.sizePage;
						currentPage=Math.floor(currentItem/c.sizePage);
						next.addClass('disableArrow');
					}
					f.getMoveBlock().animate({left:-currentItem*f.getWidthItem()});
					positionMoveBlock=currentItem*f.getWidthItem();
				},

				hoverTime:function(){
					timeOut=setInterval(function(){
						if(pagePosition>f.getSizeNextScroll()){
							if(!(f.getMoveBlock().position().left <= -f.getLastPage())){positionMoveBlock+=c.sizeScroll; prev.removeClass('disableArrow')}
							else{positionMoveBlock=f.getLastPage(); next.addClass('disableArrow')}
						}
						if(pagePosition<f.getSizePrevScroll()){
							if(!(f.getMoveBlock().position().left >= 0)){positionMoveBlock-=c.sizeScroll; next.removeClass('disableArrow')}
							else{positionMoveBlock=0; prev.addClass('disableArrow')}
							
						}
						f.getMoveBlock().css({left:-positionMoveBlock});
						currentItem=Math.floor(positionMoveBlock/f.getWidthItem());
						currentPage=Math.floor(currentItem/c.sizePage);
					}, 2)
				},
				opacityItem:function(){
					f.getMoveBlock().find('img').each(function(){
						$(this).css({opacity:0.7})
							.hover(function(){$(this).css({opacity:1})},function(){$(this).css({opacity:0.7})});
					})
				},
				onStart:function(){
					var s;
					f.getClip().addClass('clipScriptLoad');
					f.getMoveBlock().addClass('listScriptLoad');
					f.opacityItem();
					if(f.getWidthItem()*f.getAllItem().length>f.getClip().width()){
						o.prepend(prev);
						o.append(next);
						if(f.getAllItem().length%2){
							s=(f.getWidthItem()*(f.getAllItem().length+1))/2-(f.getClip().width()/2);
						}else{
							s=(f.getWidthItem()*f.getAllItem().length)/2-(f.getClip().width()/2);
						}
						f.getMoveBlock().css({left:-s});
						positionMoveBlock=s;
						f.bindHoverScroll();
						prev.bind('click', function(){
							next.removeClass('disableArrow');
							f.movePage(--currentPage);
						});
						next.bind('click', function(){
							prev.removeClass('disableArrow');
							f.movePage(++currentPage);
						});
					}else{
						s=0;
						f.getMoveBlock().css({left:-s});
					}
				},
				bindHoverScroll:function(){
					f.getClip().bind('mouseenter',function(){
						f.hoverTime();
					}).bind('mouseleave',function(){clearInterval(timeOut)}).bind('mousemove', function(e){pagePosition=e.pageX});
				}
			});
			f.onStart();
		})
	}
})(jQuery);
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);
(function(){var e=typeof jQuery=="function";var i={width:"100%",height:"100%",allowfullscreen:true,allowscriptaccess:"always",quality:"high",version:null,onFail:null,expressInstall:null,w3c:false,cachebusting:false};if(e){jQuery.tools=jQuery.tools||{};jQuery.tools.flashembed={version:"1.0.4",conf:i}}function j(){if(c.done){return false}var l=document;if(l&&l.getElementsByTagName&&l.getElementById&&l.body){clearInterval(c.timer);c.timer=null;for(var k=0;k<c.ready.length;k++){c.ready[k].call()}c.ready=null;c.done=true}}var c=e?jQuery:function(k){if(c.done){return k()}if(c.timer){c.ready.push(k)}else{c.ready=[k];c.timer=setInterval(j,13)}};function f(l,k){if(k){for(key in k){if(k.hasOwnProperty(key)){l[key]=k[key]}}}return l}function g(k){switch(h(k)){case"string":k=k.replace(new RegExp('(["\\\\])',"g"),"\\$1");k=k.replace(/^\s?(\d+)%/,"$1pct");return'"'+k+'"';case"array":return"["+b(k,function(n){return g(n)}).join(",")+"]";case"function":return'"function()"';case"object":var l=[];for(var m in k){if(k.hasOwnProperty(m)){l.push('"'+m+'":'+g(k[m]))}}return"{"+l.join(",")+"}"}return String(k).replace(/\s/g," ").replace(/\'/g,'"')}function h(l){if(l===null||l===undefined){return false}var k=typeof l;return(k=="object"&&l.push)?"array":k}if(window.attachEvent){window.attachEvent("onbeforeunload",function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){}})}function b(k,n){var m=[];for(var l in k){if(k.hasOwnProperty(l)){m[l]=n(k[l])}}return m}function a(r,t){var q=f({},r);var s=document.all;var n='<object width="'+q.width+'" height="'+q.height+'"';if(s&&!q.id){q.id="_"+(""+Math.random()).substring(9)}if(q.id){n+=' id="'+q.id+'"'}if(q.cachebusting){q.src+=((q.src.indexOf("?")!=-1?"&":"?")+Math.random())}if(q.w3c||!s){n+=' data="'+q.src+'" type="application/x-shockwave-flash"'}else{n+=' classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'}n+=">";if(q.w3c||s){n+='<param name="movie" value="'+q.src+'" />'}q.width=q.height=q.id=q.w3c=q.src=null;for(var l in q){if(q[l]!==null){n+='<param name="'+l+'" value="'+q[l]+'" />'}}var o="";if(t){for(var m in t){if(t[m]!==null){o+=m+"="+(typeof t[m]=="object"?g(t[m]):t[m])+"&"}}o=o.substring(0,o.length-1);n+='<param name="flashvars" value=\''+o+"' />"}n+="</object>";return n}function d(m,p,l){var k=flashembed.getVersion();f(this,{getContainer:function(){return m},getConf:function(){return p},getVersion:function(){return k},getFlashvars:function(){return l},getApi:function(){return m.firstChild},getHTML:function(){return a(p,l)}});var q=p.version;var r=p.expressInstall;var o=!q||flashembed.isSupported(q);if(o){p.onFail=p.version=p.expressInstall=null;m.innerHTML=a(p,l)}else{if(q&&r&&flashembed.isSupported([6,65])){f(p,{src:r});l={MMredirectURL:location.href,MMplayerType:"PlugIn",MMdoctitle:document.title};m.innerHTML=a(p,l)}else{if(m.innerHTML.replace(/\s/g,"")!==""){}else{m.innerHTML="<h2>Flash version "+q+" or greater is required</h2><h3>"+(k[0]>0?"Your version is "+k:"You have no flash plugin installed")+"</h3>"+(m.tagName=="A"?"<p>Click here to download latest version</p>":"<p>Download latest version from <a href='http://www.adobe.com/go/getflashplayer'>here</a></p>");if(m.tagName=="A"){m.onclick=function(){location.href="http://www.adobe.com/go/getflashplayer"}}}}}if(!o&&p.onFail){var n=p.onFail.call(this);if(typeof n=="string"){m.innerHTML=n}}if(document.all){window[p.id]=document.getElementById(p.id)}}window.flashembed=function(l,m,k){if(typeof l=="string"){var n=document.getElementById(l);if(n){l=n}else{c(function(){flashembed(l,m,k)});return}}if(!l){return}if(typeof m=="string"){m={src:m}}var o=f({},i);f(o,m);return new d(l,o,k)};f(window.flashembed,{getVersion:function(){var m=[0,0];if(navigator.plugins&&typeof navigator.plugins["Shockwave Flash"]=="object"){var l=navigator.plugins["Shockwave Flash"].description;if(typeof l!="undefined"){l=l.replace(/^.*\s+(\S+\s+\S+$)/,"$1");var n=parseInt(l.replace(/^(.*)\..*$/,"$1"),10);var r=/r/.test(l)?parseInt(l.replace(/^.*r(.*)$/,"$1"),10):0;m=[n,r]}}else{if(window.ActiveXObject){try{var p=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7")}catch(q){try{p=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");m=[6,0];p.AllowScriptAccess="always"}catch(k){if(m[0]==6){return m}}try{p=new ActiveXObject("ShockwaveFlash.ShockwaveFlash")}catch(o){}}if(typeof p=="object"){l=p.GetVariable("$version");if(typeof l!="undefined"){l=l.replace(/^\S+\s+(.*)$/,"$1").split(",");m=[parseInt(l[0],10),parseInt(l[2],10)]}}}}return m},isSupported:function(k){var m=flashembed.getVersion();var l=(m[0]>k[0])||(m[0]==k[0]&&m[1]>=k[1]);return l},domReady:c,asString:g,getHTML:a});if(e){jQuery.fn.flashembed=function(l,k){var m=null;this.each(function(){m=flashembed(this,l,k)});return l.api===false?this:m}}})();