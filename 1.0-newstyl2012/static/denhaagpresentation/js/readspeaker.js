/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                               */
/*                                                                          */
/* Filename:                $File: readspeaker.js $               */
/* Last changed by:         $Author: bslokxr $                              */
/* Last modified:           $Date: 2011-12-14 09:02:47 +0100 (Wed, 14 Dec 2011) $                                          */
/* Revision number:         $Rev: 31 $                                      */
/*                                                                          */
/****************************************************************************/

function getSelectedHTML() {
	selectedString="";
	var rng=undefined;
	if (window.getSelection) {
		selobj = window.getSelection();
		if (!selobj.isCollapsed) {
			if (selobj.getRangeAt) {
				rng=selobj.getRangeAt(0);
			}
			else {
				rng = document.createRange();
				rng.setStart(selobj.anchorNode,selobj.anchorOffset);
				rng.setEnd(selobj.focusNode,selobj.focusOffset);
			}
			if (rng) {
				DOM = rng.cloneContents();
				object = document.createElement('div');
				object.appendChild(DOM.cloneNode(true));
				selectedString=object.innerHTML;
			}
			else {
				selectedString=selobj;
			}
		}
	}
	else if (document.selection) {
		selobj = document.selection;
		rng = selobj.createRange();
		if (rng && rng.htmlText) {
			selectedString = rng.htmlText;
		}
		else if (rng && rng.text) {
			selectedString = rng.text;
		}
	}
	else if (document.getSelection) {
		selectedString=document.getSelection();
	}
	
	document.getElementById('rs_form').selectedhtml.value = selectedString;
	if (document.getElementById('rs_form').url) {
		if (!document.getElementById('rs_form').url.value) {
			if (window.location.href) {
				document.getElementById('rs_form').url.value=window.location.href;
			}
			else if (document.location.href) {
				document.getElementById('rs_form').url.value=document.location.href;
			}
		}
	}
}

function copyselected() {
	setTimeout("getSelectedHTML()",50);
	return true;
}

function openAndRead() {
	document.getElementById('rs_button').setAttribute("target","rs");
	document.getElementById('rs_form').setAttribute("target","rs");
    var mywindow = window.open("","rs","width=310,height=120,toolbar=0");
    setTimeout("document.getElementById('rs_form').submit();",500);
}

var selectedString="";
document.onmouseup = copyselected;
document.onkeyup = copyselected;
