/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                               */
/*                                                                          */
/* Filename:                $File: lib.js $               */
/* Last changed by:         $Author: bslokxr $                              */
/* Last modified:           $Date: 2012-06-11 11:36:41 +0200 (Mon, 11 Jun 2012) $                                          */
/* Revision number:         $Rev: 124 $                                      */
/*                                                                          */
/****************************************************************************/

var img;
var img_mo;
var img_cl;
img = new Array();
img_mo = new Array();
img_cl = new Array();

function initMo(uniqueid, origImgSrc, overImgSrc, clickImgSrc) {
  if (origImgSrc != '') {
    img[uniqueid] = new Image();
    img[uniqueid].src = origImgSrc;
  }
  if (overImgSrc != '') {
    img_mo[uniqueid] = new Image();
    img_mo[uniqueid].src = overImgSrc;
  }
  if (clickImgSrc != '') {
    img_cl[uniqueid] = new Image();
    img_cl[uniqueid].src = clickImgSrc;
  }
}


function mov(uniqueid) {
  if (img_mo[uniqueid]) {
	if (typeof document[uniqueid] != 'undefined') {
    	document[uniqueid].src = img_mo[uniqueid].src;
    } else {
    	obj = document.getElementById(uniqueid);
    	if (typeof obj != 'undefined') {
	    	obj.src = img_mo[uniqueid].src;
    	}
    }
  }
}


function mou(uniqueid) {
  if(img[uniqueid]) {
	if (typeof document[uniqueid] != 'undefined') {
		document[uniqueid].src = img[uniqueid].src;
    } else {
    	obj = document.getElementById(uniqueid);
    	if (typeof obj != 'undefined') {
		    obj.src = img[uniqueid].src;
    	}
    }
  }
}


function md(uniqueid) {
  if (img_cl[uniqueid]) {
	if (typeof document[uniqueid] != 'undefined') {
	    document[uniqueid].src = img_cl[uniqueid].src;
    } else {
    	obj = document.getElementById(uniqueid);
    	if (typeof obj != 'undefined') {
		    obj.src = img_cl[uniqueid].src;
    	}
    }
  }
}

function setCookie(name, value, expire) {
  if (expire == '') {
    document.cookie = name + '=' + escape(value) + '; path=/';
  } else {
    var expires = new Date();
    expires.setTime(expires.getTime() + expire);

    document.cookie = name + '=' + escape(value) + ((expire == null) ? '' : ('; expires=' + expires.toGMTString())) + '; path=/';
  }
}


function getCookie(name) {
   var search = name + "=";
   var val = "";
   var offset,end;
   
   if(document.cookie.length > 0) { // if there are any cookies
      offset = document.cookie.indexOf(search) 

      if(offset != -1) { // if cookie exists 
         offset += search.length;

         // set index of beginning of value
         end = document.cookie.indexOf(";", offset) 

         // set index of end of cookie value
         if (end == -1) {
            end = document.cookie.length;
         }

         val = unescape(document.cookie.substring(offset, end));
      } 
   }

   return val;
}

function newSession() {
  if (getCookie('session') == 'set') {
    return false;
  } else {
    setCookie('session', 'set', '');
    if (getCookie('session') == 'set') {
      return true;
    } else {
      return false;
    }
  }
}

function showURL(url, windowname, width, height) {
  var w;
  w = top.open(url, windowname, 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,height=' + height + ',width=' + width);
  if(w==null) {
    return null;
  }
  w.opener = window;
  return w;
}

var requiredVersion = 5; 
var useRedirect = false; 
           
var flash2Installed = false;  
var flash3Installed = false;  
var flash4Installed = false;  
var flash5Installed = false;  
var flash6Installed = false;
var flash7Installed = false;
var flash8Installed = false;
var flash9Installed = false;
var flash10Installed = false;

var maxVersion = 10;   
var actualVersion = 0;    
var hasRightVersion = false;  
var jsVersion = 1.0;    

var isIE = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;   // true if we're on ie
var isWin = (navigator.appVersion.indexOf("Windows") != -1) ? true : false; // true if we're on windows

if(isIE && isWin){ // don't write vbscript tags on anything but ie win
  document.write('<SCR' + 'IPT LANGUAGE=VBScript\> \n');
  document.write('on error resume next \n');
  document.write('flash2Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.2"))) \n');
  document.write('flash3Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.3"))) \n');
  document.write('flash4Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.4"))) \n');
  document.write('flash5Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.5"))) \n');
  document.write('flash6Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.6"))) \n');
  document.write('flash7Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.7"))) \n');
  document.write('flash8Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.8"))) \n');
  document.write('flash9Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.9"))) \n');
  document.write('flash10Installed = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.10"))) \n');
  document.write('</SCR' + 'IPT\> \n'); // break up end tag so it doesn't end our script
}

jsVersion = 1.1;
detectFlash = function(){
  if (navigator.plugins) {  // does navigator.plugins exist?
    if (navigator.plugins["Shockwave Flash 2.0"]  
  || navigator.plugins["Shockwave Flash"]){   

    var isVersion2 = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
    var flashDescription = navigator.plugins["Shockwave Flash" + isVersion2].description;
    var flashVersion = parseInt(flashDescription.charAt(flashDescription.indexOf(".") - 1));

    flash2Installed = flashVersion == 2;
    flash3Installed = flashVersion == 3;
    flash4Installed = flashVersion == 4;
    flash5Installed = flashVersion == 5;
    flash6Installed = flashVersion == 6;
    flash7Installed = flashVersion == 7;
    flash8Installed = flashVersion == 8;
    flash9Installed = flashVersion == 9;
    flash10Installed = flashVersion == 0;
    }
  }

  for (var i = 2; i <= maxVersion; i++) {
    if (eval("flash" + i + "Installed") == true) actualVersion = i;
  }

  if(navigator.userAgent.indexOf("WebTV") != -1) actualVersion = 2;

  if (actualVersion >= requiredVersion) {
    hasRightVersion = true;
    if (useRedirect) {
      if(jsVersion > 1.0) {
        window.location.replace(flashPage);
      } else {
        window.location = flashPage;
      }
    }
  } else {  
    if (useRedirect) {    
      if(jsVersion > 1.0) { 
        window.location.replace((actualVersion >= 2) ? upgradePage : noFlashPage);
      } else {
        window.location = (actualVersion >= 2) ? upgradePage : noFlashPage;
      }
    }
  }
}
 
writeFlash = function(s,w,h,a) {
  
  var alternateContent = a;
  if (alternateContent == '') {
    alternateContent = 'Voor deze site heeft u <a target="_blank" href="http://www.microsoft.com/download/">Microsoft Internet Explorer</a> versie 5.5 (of hoger) en de <a target="_blank" href="http://www.macromedia.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&P5_Language=English">Macromedia Flash plugin</a> versie ' + requiredVersion + ' (of hoger) nodig.'
  }

  var flashContent = '<OBJECT CLASSID="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" WIDTH="'+w+'" HEIGHT="'+h+'" CODEBASE="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab">'
  + '<PARAM NAME="MOVIE" VALUE="'+s+'">'
  + '<PARAM NAME="PLAY" VALUE="true">'
  + '<PARAM NAME="LOOP" VALUE="false">'
  + '<PARAM NAME="QUALITY" VALUE="high">'
  + '<PARAM NAME="MENU" VALUE="false">'
  + '<EMBED SRC="'+s+'" WIDTH="'+w+'" HEIGHT="'+h+'" PLAY="true" LOOP="false" QUALITY="high" MENU="false" TYPE="application/x-shockwave-flash" PLUGINSPAGE="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash"></EMBED>'
  + '<NOEMBED>' + alternateContent + '</NOEMBED></OBJECT>';

  if (hasRightVersion) {
    document.write(flashContent);
  } else {
    document.write(alternateContent);
  }
}

// addOnLoadListener(func)
// ~~~~~~~~~~~~~~~~~~~~~~~
// Adds a function to be called during the window's onLoad event. Multiple
// functions may be registered. This function is intended to be called during
// document load-time.
// For example:
//
// <body>
//   <script language="javascript1.2">
//     function task1() { alert('task1'); }
//     addOnLoadListener(task1);
//   </script>
// </body>
//
// Tested on IE 6.0 and Mozilla 1.5.
//
// Parameters:
// func - Function to be called during the window's onLoad event.

function addOnLoadListener(func) {
  if (window.addEventListener) {
    window.addEventListener("load", func, true);
  } else if (window.attachEvent) {
    window.attachEvent("onload", func);   
  }
}



/**
 *  TODO: refactor this to a proper JQUERY solution or into a servlet back-end solution / GM
 *  ****************************************************************************************
 *
 *  pagehistory.js
 *
 *  Manages the page history list.
 *
 *  @author   StijnW <Stijn.de.Witt@gx.nl>
 *  @date     2007/01/30
 *
 *  Copyright 2007 (c) by <GX> Creative Online Development B.V
 *  All rights reserved.
 *  GX: Open For Business
 *
 *  USAGE:
 *  -----
 *  At the top of your page (preferably in the head section of the document),
 *  create an instance of the PageHistory 'class'. Pass your current page as a
 *  parameter if it should be included in the list (it will only show up in 
 *  the next request):
 *
 *  <head>
 *    <script type="text/javascript" src="pagehistory.js"></script>
 *    <script type="text/javascript">
 *      var pageHistory = new PageHistory(
 *          new Page("My Page", "/mypage.html", "Click to go to my page"));
 *    </script>
 *
 *  At the position in your document where you want the history list to 
 *  appear, place an HTML element with a unique id:
 *
 *  <ul class="pageHistory" id="pageHistoryList">
 *    <!-- populated from javascript -->
 *  </ul>
 *
 *  At the bottom of your document, preferably just before the end of the body
 *  section, call the PageHistory member function populateList, passing the
 *  id of your element and the item HTML as a pattern:
 *
 *    <script type="text/javascript">
 *      pageHistory.populateList("pageHistoryList", 
 *          "<li><a href=\"@url@\" id=\"item@idx@\" title="@hint@">@title@</a></li>");
 *    </script>
 *  </body>
 *
 *  You can use the following pattern parameters in your HTML pattern:
 *  @title@ = Title of the page
 *  @url@   = URL of the page
 *  @hint@  = Popup hint for the page
 *  @idx@   = Index of the current item in the list
 *  
 *  IMPLEMENTATION DETAILS:
 *  ----------------------
 *  The page history is stored in a clientside cookie in a 
 *  String of the following format:
 *  <page>]|[<page>]|[<page>
 *  The string ']|[' is used as a separator. 
 *  <page> is itself an encoded string with this format:
 *  <title>}|{<url>}|{<hint>
 *  The string '}|{' is used as a separator.
 *  Example:
 *  "Home}|{/web/show/id=12345}|{Back to the homepage]|[Shop}|{/web/show/id=23456}|{Visit our shop"
 */

var PAGEHISTORY_COOKIE = "PAGEHISTORY";
var PAGEHISTORY_NOTITLE = "Geen titel";
var PAGEHISTORY_NOURL = "#";
var PAGEHISTORY_SEP_PAGE = "]|[";
var PAGEHISTORY_SEP_PART = "}|{";
var PAGEHISTORY_MAXPAGES = 3;

/**
 *  Constructor for the Page 'class'
 *
 *  @param  title   Title of the page
 *  @param  url     URL of the page. Use relative URL's whenever possible.
 *  @param  hint    Popup hint for the page.
 *
 *  Examples:
 *    var page = new Page("My Page", "/mypage.html");
 *    var page = new Page("My Page", "/mypage.html", "Click here");
 */
function Page(title, url, hint)
{
  this.title = title;
  if (this.title == null)
    this.title = PAGEHISTORY_NOTITLE;
  this.url = url;
  if (this.url == null)
    this.url = PAGEHISTORY_NOURL;
  this.hint = hint;

  this.toString = Page_toString;
}

function Page_toString()
{
  return this.title + PAGEHISTORY_SEP_PART + this.url + 
      (this.hint != null ? PAGEHISTORY_SEP_PART + this.hint : "");
}

/**
 *  Constructor for the PageHistory 'class'
 *
 *  @param  currentPage   A Page with the title and URL of the current page
 *                        if it should be added to the page history list, or 
 *                        null if it does not have to be added.
 *  @param  maxPages      Maximum number of pages to keep in the history list.
 *
 *  Examples:
 *    var pageHistory = new PageHistory(new Page("my page", "/mypage.html"));
 *    var pageHistory = new PageHistory(null);
 *    var pageHistory = new PageHistory();
 *    var myPage = new Page("my page", "/mypage.html", "Click here");
 *    var pageHistory = new PageHistory(myPage);
 */
function PageHistory(currentPage, maxPages)
{
  this.pages = new Array();
  this.currentPage = currentPage;

  this.maxPages = PAGEHISTORY_MAXPAGES;
  if (maxPages != null)
    this.maxPages = maxPages;

  // Alias methods
  this.loadPageHistory = PageHistory_loadPageHistory;
  this.savePageHistory = PageHistory_savePageHistory;
  this.toString = PageHistory_toString;
  this.populateList = PageHistory_populateList;

  // Load page history from cookie
  this.loadPageHistory(PAGEHISTORY_COOKIE);
  // The pagehistory is loaded. Now immediately save it. This
  // wil store our currentPage in the list if it was set, so it
  // will be available in the next request.
  this.savePageHistory(PAGEHISTORY_COOKIE);
}


function PageHistory_toString()
{
  var result = "";
  var maxPages = this.maxPages;
  if (this.currentPage != null)
  {
    // Check if the current page is already in the list
    var isInList = false;
    for (var i=0; i<this.pages.length; i++)
      if (this.pages[i].url == this.currentPage.url)
        isInList = true;

    // Only add the current page if it's not in the list already
    if (! isInList)
    {
      maxPages -= 1;
      result += this.currentPage;
    }
  }

  for (var i=0; (i<this.pages.length) && (i<maxPages); i++)
  {
    var page = this.pages[i];
    if (result != "")
      result += PAGEHISTORY_SEP_PAGE;
    result += page;
  }
  return result;
}


/**
 *  Populates the page history list with one item for each page in the list.
 *
 *  @param  listId        The id of the HTML element that should be populated.
 *                        Does not necesarilly have to be a list element.
 *  @param  itemPattern   A pattern that defines how the list items will be
 *                        created, pattern parameters will automatically be
 *                        filled in for you. Available parameters:
 *                            @title@ = Title of the page
 *                            @url@   = URL of the page
 *                            @hint@  = Popup hint for the page
 *                            @idx@   = Index of the current item in the list
 *                        
 *  Examples: 
 *    var pattern = "<li><a href=\"@url@\" id=\"item@idx@\" title="@hint@">@title@</a></li>";
 *    pageHistory.populateList("myHistoryList", pattern);
 *
 */
function PageHistory_populateList(listId, itemPattern)
{
  if ((listId == null) || (itemPattern == null))
    return false;

  list = document.getElementById(listId);
  if (list)
  {
    var html = "";
    for (var i=0; i<this.pages.length; i++)
    {
      var item = itemPattern;
      item = item.replace(/@title@/g, this.pages[i].title);
      item = item.replace(/@url@/g, this.pages[i].url);
      if (this.pages[i].hint != null)
        item = item.replace(/@hint@/g, this.pages[i].hint);
      else
        item = item.replace(/@hint@/g, "");
      item = item.replace(/@idx@/g, i);
      html += item + "\n";
    }
    list.innerHTML = html;
  }
}

/**
 *  Loads the page history from a cookie.
 *
 *  @param  cookieName  The name of the cookie to load from.
 */
function PageHistory_loadPageHistory(cookieName)
{
  var cookieValue = null;

  // Code 'borrowed' from Quirksmode.org, thanks Peter-Paul Koch!
  // http://www.quirksmode.org/js/cookies.html
  var nameEQ = cookieName + "=";
  var cookies = document.cookie.split(';');
  for(var i=0; i<cookies.length; i++) 
  {
    var c = cookies[i];

    while (c.charAt(0) == ' ')
      c = c.substring(1, c.length);

    if (c.indexOf(nameEQ) == 0) 
    {
      cookieValue = unescape(c.substring(nameEQ.length, c.length));
      break;
    }
  }

  if (cookieValue != null)
  {
    pagesStrings = cookieValue.split(PAGEHISTORY_SEP_PAGE);
    for (var i=0; i<pagesStrings.length; i++)
    {
      pageString = pagesStrings[i];
      pageParts = pageString.split(PAGEHISTORY_SEP_PART);
      if (pageParts.length >= 2)
      {
        pageTitle = pageParts[0];
        pageUrl = pageParts[1];
        pageHint = null;
        if (pageParts.length == 3)
          pageHint = pageParts[2];
          
        this.pages[this.pages.length] = new Page(pageTitle, pageUrl, pageHint);
      }
    }
  }
}  

/**
 *  Saves the page history to a cookie.
 *
 *  @param  cookieName  The name of the cookie to save to.
 */
function PageHistory_savePageHistory(cookieName)
{
  document.cookie = cookieName + "=" + escape(this.toString()) + "; path=/";
}

$(document).ready(function() {
  var pageHistory = new PageHistory(new Page($('title').text().replace("Den Haag - ",""), window.location.pathname));
   pageHistory.populateList("pageHistoryList", "<li><a href=\"@url@\" id=\"item@idx@\" title=\"@title@\">@title@</a></li>");
});