/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                               */
/*                                                                          */
/* Filename:                $File: qgoclick.js $               */
/* Last changed by:         $Author: bslokxr $                              */
/* Last modified:           $Date: 2011-12-14 09:02:47 +0100 (Wed, 14 Dec 2011) $                                          */
/* Revision number:         $Rev: 31 $                                      */
/*                                                                          */
/****************************************************************************/

var xmlHttp;
var noMatchLink;

function doQgoClick(handleId, questionId, answerId, alias, siteRef, keywordsession) {
	xmlHttp = GetXmlHttpObject();

	if (xmlHttp == null) {
		// unable to register the click since this browser does not support http request.
		return;
	} 

	var url = "/web/wcbservlet/com.gxwebmanager.gxpublic.qgoelement.servlet";
	url = url + "?clicktype=qgo";
	url = url + "&handleid=" + handleId;
	url = url + "&questionid=" + questionId;
	url = url + "&answerid=" + answerId;
	url = url + "&alias=" + alias;
	url = url + "&siteref=" + siteRef;
	url = url + "&keywordsession=" + keywordsession;
	xmlHttp.onreadystatechange = stateChanged;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function doQgoNoMatchClick(handleId, link) {
	noMatchLink = link;
	xmlHttp = GetXmlHttpObject();

	if (xmlHttp == null) {
		// unable to register the click since this browser does not support http request.
		return;
	} 

	var url = "/web/wcbservlet/com.gxwebmanager.gxpublic.qgoelement.servlet";
	url = url + "?clicktype=nomatch";
	url = url + "&handleid=" + handleId;
	xmlHttp.onreadystatechange = noMatchStateChanged;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function noMatchStateChanged() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		// click is submitted to serlvet and received a 200 response code.
		window.location = noMatchLink;
	}
}

function doLinkClick(keyword, pageTitle, linkurl, alias, siteRef, handleId) {
	xmlHttp = GetXmlHttpObject();

	if (xmlHttp == null) {
		// unable to register the click since this browser does not support http request.
		return;
	} 

	var url = "/web/wcbservlet/com.gxwebmanager.gxpublic.qgoelement.servlet";
	url = url + "?clicktype=link";
	url = url + "&keyword=" + keyword;
	url = url + "&pagetitle=" + pageTitle;
	url = url + "&handleid=" + handleId;
	if (typeof escape == 'function') {
		url = url + "&url=" + escape(linkurl);
	} else {
		url = url + "&url=" + linkurl;
	}
	url = url + "&alias=" + alias;
	if (siteRef != 'undefined') {
		url = url + "&siteref=" + siteRef;
	}
	xmlHttp.onreadystatechange = stateChanged;
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function stateChanged() {
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
		// click is submitted to serlvet and received a 200 response code.
	}
}

function GetXmlHttpObject() {
	var xmlHttp = null;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function updateSearchResultsLinks() {
	var allElements = document.getElementsByClassName('qgoclick');
	for (var i = 0; i < allElements.length; i++) {
		var link = allElements[i].href;
		allElements[i].href = '#';
		allElements[i].onclick = allElements[i].onclick + '; document.href='+ link + ';';
	}
}

function addLoadEvent(func) {
	var oldonload = window.onload;
	if (typeof window.onload != 'function') {
		window.onload = func;
	} else {
		window.onload = function() {
			func();
			if (oldonload) {
				oldonload();
			}
		}
	}
}
addLoadEvent(updateSearchResultsLinks);