/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                               */
/*                                                                          */
/* Filename:                $File: reportabuse.js $               */
/* Last changed by:         $Author: bslokxr $                              */
/* Last modified:           $Date: 2011-12-14 09:02:47 +0100 (Wed, 14 Dec 2011) $                                          */
/* Revision number:         $Rev: 31 $                                      */
/*                                                                          */
/****************************************************************************/

function reportAbuse(postingId, forumName, content, date) {				
	var postingIdField = document.getElementById('posting_postingId');
	var forumNameField = document.getElementById('posting_forumName');
	var contentField = document.getElementById('posting_content');			
	var dateField = document.getElementById('posting_date');
	
	postingIdField.value = 'abuse-'+postingId+'-';
	forumNameField.value = forumName;
	contentField.value = content;
	dateField.value = date;

	var form = document.getElementById('reportAbuse');
	form.submit();
}