/****************************************************************************/
/*                                                                          */
/* www.denhaag.nl/                                               */
/*                                                                          */
/* Filename:                $File:  $               */
/* Last changed by:         $Author:  $                              */
/* Last modified:           $Date:  $                                          */
/* Revision number:         $Rev:  $                                      */
/*                                                                          */
/****************************************************************************/
/**
 * Settings
**/

var leaveMessage = 'De pagina die u gaat bezoeken, is misschien niet geschikt voor uw apparaat. Wilt u doorgaan?';


/**
 * Match all external links, exclude whitelist
**/
jQuery.expr[':'].external = function(obj){
	return !obj.href.match(/^mailto\:/)
		&& !obj.hostname.endsWith('soacentrumdenhaag.nl')
		&& !obj.hostname.endsWith('facebook.com')
		&& !obj.hostname.endsWith('twitter.com')
		&& !obj.hostname.endsWith('linkedin.com')
		&& !obj.hostname.endsWith('hyves.nl')
		&& !obj.hostname.endsWith('nujij.nl')
		&& !obj.hostname.endsWith('formulier.denhaag.nl')
		&& !(obj.hostname == window.location.hostname && obj.href.indexOf('channel=touch') != -1)
		&& !(obj.hostname == window.location.hostname && obj.href.indexOf('#') != -1)
		&& !(obj.hostname.endsWith('denhaag.nl') && obj.href.indexOf('channel=touch') != -1);
};

jQuery.expr[':'].externalform = function(obj){
	return	obj.action.endsWith('zoekdienst.overheid.nl/Zoekdienst/html.onl.nl/collectie/dcr?') ||
		obj.action.endsWith('www.haagsuitburo.nl/events/search');
};

$(document).ready(function() {
	// Load message
	$('.leaveMessage p').html(leaveMessage);

	/* For all external links, fadein leaveMessage and copy href and target to OK button */
	$('a:external').click(function() {
		trimmedHref = $.trim($(this).attr('href'));
		if(trimmedHref.indexOf('javascript:') === 0 || trimmedHref == '') return true;
		$('.leaveMessage, .leaveMessageOverlay')
			.fadeIn('slow')
			.find('.ok')
			.attr('href', $(this).attr('href'))
			.attr('target', $(this).attr('target'));
		return false;
	});

	$('form:externalform').submit(function(e) {

		if($(this).attr('data-confirmed') == 1) return true;

		clickedForm = $(this);

		$('.leaveMessage, .leaveMessageOverlay')
			.fadeIn('slow')
			.find('.ok')
			.click(function() {
				clickedForm.attr('data-confirmed', 1);
				clickedForm.submit();
			});
		return false;
	});

});