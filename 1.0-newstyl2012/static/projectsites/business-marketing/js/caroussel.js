var caroussel = caroussel || {};

caroussel.init = function(){

	// this.isIpad = false;
	this.isIpad = (navigator.platform.indexOf("iPad") != -1);
	this.currentPage = 0;
	this.numberOfPages = 0;
	this.container = $("#caroussel .content ul");
	this.animationSpeed = 250;

	$(window).bind("resize", function(){
		caroussel.resize();
	}).trigger("resize");
	
	// change no-javascript-fallback-css
	$("#caroussel .content").css({
		"overflow": "hidden",
		"height": 388
	}).find("> .rollover").each(function(){
		$(this).css({
			"top": "",
			"bottom": -$(this).height()
		});
	});
	$("#caroussel .header").css("margin-bottom", 11);
	$("#caroussel .navigation").css("display", "block");

	// show .rollover on mouseover
	$("#caroussel .content li").each(function(){
		$(this).bind("mouseover", function(){
			var rollover = $(this).find("> .rollover");
			rollover.stop().animate({
				"top": 388 - rollover.height()
			}, caroussel.animationSpeed);
		}).bind("mouseout", function(){
			$(this).find("> .rollover").stop().animate({
				"top": 388
			}, caroussel.animationSpeed);
		});
	});

	// create navigation for caroussel
	this.numberOfPages = $("#caroussel .content li").length / 3;
	for (var i = 0; i < this.numberOfPages; i++) {
		var a = $("<a>").attr("id", "caroussel-link-"+i).bind("click", function(){
			var id = i;
			return function(){
				caroussel.showPage(id);
			};
		}()).text(i+1).insertBefore("#caroussel .navigation a.next");
	}
	// $("#caroussel .navigation a:first-child").css("margin-left", (932 - (((this.numberOfPages + 2) * 28) - 10)) / 2);
	this.highlightCurrentPage();
	
}

caroussel.next = function(){
	this.currentPage++;
	this.animateToPage();
}

caroussel.previous = function(){
	this.currentPage--;
	this.animateToPage();
}

caroussel.showPage = function(id){
	this.currentPage = id;
	this.animateToPage();
}

caroussel.animateToPage = function(){
	if (this.currentPage < 0) this.currentPage = this.numberOfPages - 1;
	if (this.currentPage >= this.numberOfPages) this.currentPage = 0;
	this.container.animate({
		"left": -(this.currentPage * 316 * 3)
	}, caroussel.animationSpeed);
	this.highlightCurrentPage();
}

caroussel.highlightCurrentPage = function(){
	$("#caroussel .navigation .current").removeClass("current");
	$("#caroussel-link-"+this.currentPage).addClass("current");
}

caroussel.resize = function(){
	if (this.isIpad || $(window).height() < 600) {
		$("#caroussel .header").css({
			"height": 130
		}).find("> img").attr("src", "/static/projectsites/business-marketing/images/header-graphic-small.gif");
	} else {
		$("#caroussel .header").css({
			"height": 308
		}).find("> img").attr("src", "/static/projectsites/business-marketing/images/header-image-large.jpg");
	}
}

$(function(){ caroussel.init(); });